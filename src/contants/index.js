import { Navigation } from 'react-native-navigation';
import { ShowNativeMessage } from '../components/NativeMessage';

export const URL_BASE = 'http://api.iperum.com.br';

export const VERIFICA_CPF_SUCESSO = 'VERIFICA_CPF_SUCESSO';
export const VERIFICA_CPF_ERRO = 'VERIFICA_CPF_ERRO';
export const VERIFICA_CNPJ_SUCESSO = 'VERIFICA_CNPJ_SUCESSO';
export const VERIFICA_CNPJ_ERRO = 'VERIFICA_CNPJ_ERRO';
export const VERIFICA_EMAIL_SUCESSO = 'VERIFICA_EMAIL_SUCESSO';
export const VERIFICA_EMAIL_ERRO = 'VERIFICA_EMAIL_ERRO';
export const VERIFICA_USUARIO_SUCESSO = 'VERIFICA_USUARIO_SUCESSO';
export const VERIFICA_USUARIO_ERRO = 'VERIFICA_USUARIO_ERRO';
export const SALVA_CLIENTE_SUCESSO = 'SALVA_CLIENTE_SUCESSO';
export const SALVA_CLIENTE_ERRO = 'SALVA_CLIENTE_ERRO';
export const ATUALIZA_CLIENTE_SUCESSO = 'ATUALIZA_CLIENTE_SUCESSO';
export const ATUALIZA_CLIENTE_ERRO = 'ATUALIZA_CLIENTE_ERRO';
export const BUSCA_CLIENTE_SUCESSO = 'BUSCA_CLIENTE_SUCESSO';
export const BUSCA_CLIENTE_ERRO = 'BUSCA_CLIENTE_ERRO';

export const GUARDA_USUARIO = 'GUARDA_USUARIO';
export const LOGIN_USUARIO = 'LOGIN_USUARIO';
export const LOGIN_USUARIO_ERROR = 'LOGIN_USUARIO_ERROR';

export const MOSTRA_ESCONDE_SALDO = 'MOSTRA_ESCONDE_SALDO';

export const BUSCA_SOLICITACOES_SUCESSO = 'BUSCA_SOLICITACOES_SUCESSO';
export const BUSCA_SOLICITACOES_ERRO = 'BUSCA_SOLICITACOES_ERRO';
export const BUSCA_CONTAS_SUCESSO = 'BUSCA_CONTAS_SUCESSO';
export const BUSCA_CONTAS_ERRO = 'BUSCA_CONTAS_ERRO';
export const CADASTRA_CONTA_SUCESSO = 'CADASTRA_CONTA_SUCESSO';
export const CADASTRA_CONTA_ERRO = 'CADASTRA_CONTA_ERRO';
export const NOVA_SOLICITACAO_SUCESSO = 'NOVA_SOLICITACAO_SUCESSO';
export const NOVA_SOLICITACAO_ERRO = 'NOVA_SOLICITACAO_ERRO';
export const SAQUE = 'S';
export const DEPOSITO = 'D';

export const LISTA_CLIENTES_SUCESSO = 'LISTA_CLIENTES_SUCESSO';
export const LISTA_CLIENTES_ERRO = 'LISTA_CLIENTES_ERRO';

export const LISTA_CARTEIRAS_SUCESSO = 'LISTA_CARTEIRAS_SUCESSO';
export const LISTA_CARTEIRAS_ERRO = 'LISTA_CARTEIRAS_ERRO';

export const LISTA_EXTRATO_SUCESSO = 'LISTA_EXTRATO_SUCESSO';
export const LISTA_EXTRATO_ERRO = 'LISTA_EXTRATO_ERRO';

export const TOKEN_TRANSF_SUCESSO = 'TOKEN_TRANSF_SUCESSO';
export const TOKEN_TRANSF_ERRO = 'TOKEN_TRANSF_ERRO';

export const TRANSF_SUCESSO = 'TRANSF_SUCESSO';
export const TRANSF_ERRO = 'TRANSF_ERRO';

export const RESET_SENHA_SUCESSO = 'RESET_SENHA_SUCESSO';
export const RESET_SENHA_ERRO = 'RESET_SENHA_ERRO';

export const START_INTERVAL = 'START_INTERVAL';

export const SHORT = 'SHORT';
export const LONG = 'LONG';

export const tokenExpirado = (title, msg, time) => {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'Login',
      },
    });
  ShowNativeMessage(title, msg, time);
};

export const BASE64 = {
  _keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=', encode: function (e) {
    var t = '';
    var n, r, i, s, o, u, a;
    var f = 0;
    e = BASE64._utf8_encode(e);
    while (f < e.length) {
      n = e.charCodeAt(f++);
      r = e.charCodeAt(f++);
      i = e.charCodeAt(f++);
      s = n >> 2;
      o = (n & 3) << 4 | r >> 4;
      u = (r & 15) << 2 | i >> 6;
      a = i & 63;
      if (isNaN(r)) {
        u = a = 64;
      } else if (isNaN(i)) {
        a = 64;
      }
      t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
    }
    return t;
  }, decode: function (e) {
    var t = '';
    var n, r, i;
    var s, o, u, a;
    var f = 0;
    e = e.replace(/[^A-Za-z0-9+/=]/g, '');
    while (f < e.length) {
      s = this._keyStr.indexOf(e.charAt(f++));
      o = this._keyStr.indexOf(e.charAt(f++));
      u = this._keyStr.indexOf(e.charAt(f++));
      a = this._keyStr.indexOf(e.charAt(f++));
      n = s << 2 | o >> 4;
      r = (o & 15) << 4 | u >> 2;
      i = (u & 3) << 6 | a;
      t = t + String.fromCharCode(n);
      if (u != 64) {
        t = t + String.fromCharCode(r);
      }
      if (a != 64) {
        t = t + String.fromCharCode(i);
      }
    }
    t = Base64._utf8_decode(t);
    return t;
  }, _utf8_encode: function (e) {
    e = e.replace(/rn/g, 'n');
    var t = '';
    for (var n = 0; n < e.length; n++) {
      var r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
      } else if (r > 127 && r < 2048) {
        t += String.fromCharCode(r >> 6 | 192);
        t += String.fromCharCode(r & 63 | 128);
      } else {
        t += String.fromCharCode(r >> 12 | 224);
        t += String.fromCharCode(r >> 6 & 63 | 128);
        t += String.fromCharCode(r & 63 | 128);
      }
    }
    return t;
  }, _utf8_decode: function (e) {
    var t = '';
    var n = 0;
    var r = c1 = c2 = 0;
    while (n < e.length) {
      r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
        n++;
      } else if (r > 191 && r < 224) {
        c2 = e.charCodeAt(n + 1);
        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
        n += 2;
      } else {
        c2 = e.charCodeAt(n + 1);
        c3 = e.charCodeAt(n + 2);
        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
        n += 3;
      }
    }
    return t;
  },
};
