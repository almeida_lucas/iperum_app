/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

export default SelectListModal = ({itens, selecionaItem}) => {

  const keyExtractor = (item, index) => index.toString();

  const selecionarItem = (item) => {
    selecionaItem(item);
    closeSelectList();
  };

  const renderItem = ({item}) => (
    <TouchOpctyPrevSelect onPress={() => selecionarItem(item)}>
      <View style={styles.listaItens}>
        <Text style={styles.itemSelecionado}>{item.text}</Text>
      </View>
    </TouchOpctyPrevSelect>
  );

  const closeSelectList = () => Navigation.dismissModal({
    animationType: 'slide-down',
  });

  return (
    <View style={styles.container}>
      <View style={styles.modalContainer}>
        <View style={styles.listaContasContainer}>
          <View style={styles.listaContasHeader}>
            <Text style={styles.enviarParaTexto}>Selecione um item:</Text>
            <TouchOpctyPrevSelect onPress={closeSelectList}>
              <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
            </TouchOpctyPrevSelect>
          </View>
          <FlatList
            style={{maxHeight: 400,}}
            data={itens}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 15,
  },
  modalContainer: {
    minHeight: 300,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
  },
  listaContasContainer: {
    alignItems: 'stretch',
  },
  listaContasHeader: {
    padding: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listaItens: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  enviarParaTexto: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  itemSelecionado: {
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  contaPrincipal: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  footer: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgMais: {
    width: 30,
    height: 30,
  },
  adicionaConta: {
    fontSize: 16,
    color: '#630851',
    marginLeft: 15,
  },
});