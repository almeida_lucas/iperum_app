/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

const SendToModal = ({contas, selecionaContaSaque}) => {

  const keyExtractor = (item, index) => index.toString();

  const selecionarConta = (conta) => {
    selecionaContaSaque(conta);
    closeSendTo();
  };

  const renderItem = ({item}) => (
    <TouchOpctyPrevSelect onPress={() => selecionarConta(item)} disabled={!selecionaContaSaque}>
      <View style={styles.listaContas}>
        <Text style={styles.contaSelecionada}>{item.nome}</Text>
        <Text style={styles.contaPrincipal}>{item.conta_principal === 'true' ? '(Principal)' : ''}</Text>
      </View>
    </TouchOpctyPrevSelect>
  );

  const mostraModalCadastraContaSaque = () => Navigation.showModal({
    screen: 'ModalCadastraContaSaque',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const closeSendTo = () => Navigation.dismissModal({
    animationType: 'slide-down',
  });

  return (
    <View style={styles.container}>
      <View style={styles.modalContainer}>
        <View style={styles.listaContasContainer}>
          <View style={styles.listaContasHeader}>
            <Text style={styles.enviarParaTexto}>Enviar para:</Text>
            <TouchOpctyPrevSelect onPress={closeSendTo}>
              <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
            </TouchOpctyPrevSelect>
          </View>
          <FlatList
            style={{maxHeight: 400,}}
            data={contas}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />
        </View>
        <TouchOpctyPrevSelect onPress={mostraModalCadastraContaSaque}>
          <View style={styles.footer}>
            <Image style={styles.imgMais} source={require('../resources/mais.png')}/>
            <Text style={styles.adicionaConta}>Adicionar outra conta</Text>
          </View>
        </TouchOpctyPrevSelect>
      </View>
    </View>
  );
};

const mapStateToProps = ({conta}) => {
  const {contas} = conta;
  return {
    contas,
  }
};

export default connect(mapStateToProps, null)(SendToModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 15,
  },
  modalContainer: {
    minHeight: 300,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
  },
  listaContasContainer: {
    alignItems: 'stretch',
  },
  listaContasHeader: {
    padding: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listaContas: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  enviarParaTexto: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  contaSelecionada: {
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  contaPrincipal: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  footer: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgMais: {
    width: 30,
    height: 30,
  },
  adicionaConta: {
    fontSize: 16,
    color: '#630851',
    marginLeft: 15,
  },
});