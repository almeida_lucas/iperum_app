/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  ActivityIndicator,
} from 'react-native';

export default ActivityIndicatorModal = (props) => {

  return (
    <Modal
      animationType='fade'
      transparent={true}
      onRequestClose={() => {}}
      {...props}>
      <View style={styles.container}>
        <ActivityIndicator size='small' color='#630851'/>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});
