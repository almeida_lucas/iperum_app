/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

export default class PresentationModal extends Component<{}> {
  state = {
    depositLater: false,
  };

  changePresentation = () => this.setState({depositLater: true});

  render() {
    const {nome, closeModal, closeModalOpenDeposit, closeModalOpenAccountTab} = this.props;
    const {depositLater} = this.state;

    if (depositLater)
      return (
        <View style={styles.containerDepositLater}>
          <Text style={[styles.titulo, styles.marginBottom]}>
            Sem problemas!
          </Text>
          <Text style={[styles.subTitulo, styles.marginBottom]}>
            Quando quiser depositar, é só ir em "Conta" e tocar o botão "Depositar"!
          </Text>
          <View style={{width: 150, alignItems: 'stretch'}}>
            <TouchOpctyPrevSelect
              onPress={closeModal}>
              <Button
                title='OK'
                backgroundColor='transparent'
                borderColor='#FFFFFF'
                borderWidth={2}
                titleSize={16}
              />
            </TouchOpctyPrevSelect>
          </View>
          <Image style={{height: 90, marginLeft: 70, marginTop: 30}}
                 source={require('../resources/flecha-diagonal.png')}/>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity onPress={closeModalOpenAccountTab}>
              <Image style={{height: 65, width: 75}}
                     source={require('../resources/tutorial-conta.png')}/>
            </TouchableOpacity>
          </View>
        </View>
      );

    return (
      <View style={styles.container}>
        <Text style={styles.titulo}>Olá, {nome}.</Text>
        <Text style={[styles.titulo, styles.marginBottom]}>
          Seja bem vind@ à Iperum
        </Text>
        <Text style={[styles.subTitulo, styles.marginBottom]}>
          Sua conta foi criada com sucesso!
        </Text>
        <Text style={[styles.subTitulo, styles.marginBottom]}>
          Para começar a utilizar as vantagens da Iperum, você precisa fazer o primeiro
          depósito na Ipeconta.
        </Text>

        <TouchOpctyPrevSelect
          onPress={closeModalOpenDeposit}>
          <Button
            title='Depositar'
            subTitle='na Ipeconta'
            backgroundColor='#5bab2e'
            alignText='flex-start'
            btImg='deposito'
            btImgStyle={{height: 50, width: 50, opacity: 1, top: 10, right: 15}}
          />
        </TouchOpctyPrevSelect>

        <TouchableOpacity onPress={this.changePresentation}>
          <Text style={[styles.linkTexto, styles.marginTop]}>
            Agora não...vou depositar depois.
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 20,
  },
  containerDepositLater: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
    padding: 20,
    paddingBottom: 0,
  },
  titulo: {
    fontSize: 23,
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  marginBottom: {
    marginBottom: 20,
  },
  marginTop: {
    marginTop: 20,
  },
  subTitulo: {
    fontSize: 16,
    color: '#FFFFFF',
  },
  linkTexto: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});