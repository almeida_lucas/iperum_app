/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  FlatList,
  Platform,
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { filtraClientes } from '../actions/ClientTransferAction';

import UserItem from '../components/UserItem';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

class PayToModal extends Component {

  state = {
    texto: '',
  };

  fechaModalPagarPara = () => Navigation.dismissModal({
    animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
  });

  keyExtractor = (item, index) => index.toString();

  selecionarConta = (cliente) => {
    this.props.selecionaContaPagamento(cliente);
    Navigation.dismissModal({
      animationType: 'slide-down',
    });
  };

  renderItem = ({item}) => (
    <TouchOpctyPrevSelect onPress={() => this.selecionarConta(item)}>
      <UserItem
        {...item}
      />
    </TouchOpctyPrevSelect>
  );

  onChengeText = (texto) => {
    const {listaClientesParam, filtraClientes} = this.props;
    this.setState({texto}, () => filtraClientes(listaClientesParam, texto));
  };

  render() {
    const {listaClientes} = this.props;
    const {texto} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.fechaModalPagarPara}>
            <Image style={styles.imgFechaModal} source={require('../resources/setaEsquerda.png')}/>
          </TouchOpctyPrevSelect>
          <TextInput
            style={styles.inputBuscaUsuario}
            placeholder='Apelido ou e-mail'
            underlineColorAndroid="transparent"
            value={texto}
            onChangeText={this.onChengeText}
            autoFocus={true}
            autoCapitalize='none'
          />
        </View>
        {texto !== '' ?
          <FlatList
            data={listaClientes}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}/>
          :
          null
        }
      </View>
    );
  }
}

const mapStateToProps = ({listaClientes}) => {
  return {
    listaClientes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    filtraClientes: (listaClientes, filtro) => dispatch(filtraClientes(listaClientes, filtro)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PayToModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    paddingTop: Platform.OS === 'ios' ? 15 : 0,
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  inputBuscaUsuario: {
    flex: 1,
    marginLeft: 20,
    height: 40,
  },
});
