/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  Platform,
} from 'react-native';

import { Navigation } from 'react-native-navigation';

import EditPerfilData from '../components/EditPerfilData';
import { connect } from 'react-redux';
import { atualizaCliente, buscaCliente } from '../actions/ClientAction';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import {
  ATUALIZA_CLIENTE_ERRO,
  ATUALIZA_CLIENTE_SUCESSO, BUSCA_CLIENTE_ERRO,
  BUSCA_CLIENTE_SUCESSO,
  LONG,
  tokenExpirado,
} from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';

class EditPerfilModal extends Component<{}> {

  state = {
    nome: '',
    telefone: '',
    valoresIniciados: false,
  };

  componentDidMount() {
    const {nome, iniciais, documento, email, usuario, telefone} = this.props.cliente;
    this.setState({
      nome,
      telefone,
    }, () => this.setState({valoresIniciados: true}));
  }

  fechaModalEditaPerfil = () => {
    Navigation.dismissModal({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  };

  onChangeNome = (nome) => this.setState({nome});
  onChangeTelefone = (telefone) => this.setState({telefone});

  salvarUsuario = () => {
    const {nome, telefone} = this.state;
    const {id, token} = this.props.cliente;
    const cliente = {nome, telefone};
    this.props.buscaCliente(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO)
          this.props.atualizaUsuario(id, token, cliente)
            .then(({type}) => {
              if (type === ATUALIZA_CLIENTE_SUCESSO) {
                this.fechaModalEditaPerfil();
                ShowNativeMessage('Dados atualizados', 'Dados do perfil atualizados com sucesso!', LONG);
              } else if (type === ATUALIZA_CLIENTE_ERRO) ShowNativeMessage('Erro', 'Erro ao atualizar dados do perfil.', LONG);
            });
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(this.props.interval);
          tokenExpirado('Erro', error, LONG);
        }
      });
  };

  render() {
    const {iniciais} = this.props.cliente;
    const {nome, usuario, telefone, valoresIniciados} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.fechaModalEditaPerfil}>
            <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
          </TouchOpctyPrevSelect>
          <Text style={styles.perfilTexto}>Editar Perfil</Text>
        </View>
        <ScrollView>
          <View style={styles.avatarContainer}>
            <View style={styles.avatarConteudo}>
              <View style={styles.iniciaisView}>
                <Text style={styles.iniciais}>{iniciais}</Text>
              </View>
            </View>
            <Text style={styles.dadosPessoaisTexto}>Dados pessoais</Text>
          </View>
          {
            valoresIniciados ?
              <View>
                <EditPerfilData label='Nome Completo' value={nome} onChangeText={this.onChangeNome}/>
                <EditPerfilData label='Celular' value={telefone} onChangeText={this.onChangeTelefone} type='cel-phone'/>
              </View>
              :
              null
          }
          <View style={styles.containerAtualizaUsuario}>
            <TouchOpctyPrevSelect
              onPress={this.salvarUsuario}>
              <Button
                title='CONFIRMAR'
                backgroundColor='#ffc210'
                titleSize={16}
              />
            </TouchOpctyPrevSelect>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({cliente, interval}) => {
  return {
    cliente,
    interval,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    atualizaUsuario: (id, token, cliente) => dispatch(atualizaCliente(id, token, cliente)),
    buscaCliente: (id, token) => dispatch(buscaCliente(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPerfilModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#eff2f3',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  imgEdita: {
    width: 25,
    height: 25,
    margin: 5,
  },
  perfilTexto: {
    marginLeft: 10,
    fontSize: 18,
    color: '#000000',
    fontWeight: 'bold',
  },
  avatarContainer: {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  avatarConteudo: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
  iniciaisView: {
    width: 100,
    height: 100,
    borderRadius: 100,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 38,
    color: '#AAAAAA',
  },
  dadosPessoaisTexto: {
    marginLeft: 15,
    marginBottom: 5,
    fontSize: 16,
    color: '#888888',
  },
  containerAtualizaUsuario: {
    marginTop: 15,
    marginBottom: 20,
    padding: 15,
    alignItems: 'stretch',
  },
});
