/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image, Platform,
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

export default ConfirmDepositModal = ({valorDeposito}) => {

  const closeConfirmDepositModal = () => Navigation.dismissAllModals();

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchOpctyPrevSelect onPress={closeConfirmDepositModal}>
          <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
        </TouchOpctyPrevSelect>
        <Text style={styles.aguardandoDepositoTexto}>Aguardando depósito</Text>
      </View>
      <View style={styles.containerDica}>
        <Text style={styles.textoDica}>Agora é só transferir o valor para a seguinte conta:</Text>
      </View>
      <View style={styles.dadosContaContainer}>
        <View style={styles.labelTextoContainer}>
          <Text style={styles.dadosContaLabel}>Banco:</Text>
          <Text style={styles.dadosContaTexto}>Banco Itau</Text>
        </View>
        <View style={styles.labelTextoContainer}>
          <Text style={styles.dadosContaLabel}>Agência:</Text>
          <Text style={styles.dadosContaTexto}>8552</Text>
        </View>
        <View style={styles.labelTextoContainer}>
          <Text style={styles.dadosContaLabel}>Conta:</Text>
          <Text style={styles.dadosContaTexto}>24343-1</Text>
        </View>
        <View style={styles.labelTextoContainer}>
          <Text style={styles.dadosContaLabel}>Titular:</Text>
          <Text style={styles.dadosContaTexto}>Iperum Serviços de Pagamentos</Text>
        </View>
        <View style={styles.labelTextoContainer}>
          <Text style={styles.dadosContaLabel}>CNPJ:</Text>
          <Text style={styles.dadosContaTexto}>30.400.143/00001-51</Text>
        </View>
      </View>
      <View style={styles.valorSaqueContainer}>
        <Text style={styles.valorSaqueLabel}>Valor:</Text>
        <Text style={styles.valorSaqueTexto}>R$ {valorDeposito}</Text>
      </View>
      <Text style={styles.atencaoTexto}>ATENÇÃO: O valor estará disponível na conta informada em até 2 dias
        úteis.</Text>
      <View style={styles.btnContainer}>
        <TouchOpctyPrevSelect
          onPress={closeConfirmDepositModal}>
          <Button
            title='OK'
            titleColor='#630851'
            backgroundColor='transparent'
            borderWidth={2}
            borderColor='#630851'
            titleSize={16}
          />
        </TouchOpctyPrevSelect>
        {/*<TouchOpctyPrevSelect
          onPress={closeConfirmDepositModal}>
          <Button
            title='COMPARTILHAR'
            backgroundColor='#630851'
          />
        </TouchOpctyPrevSelect>*/}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  aguardandoDepositoTexto: {
    marginLeft: 10,
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  containerDica: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#EEEEEE',
    padding: 15,
  },
  textoDica: {
    flex: 1,
    fontSize: 18,
    color: '#000000',
  },
  imgDica: {
    width: 25,
    height: 25,
  },
  dadosContaContainer: {
    padding: 15,
    paddingBottom: 0,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  labelTextoContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  dadosContaLabel: {
    color: '#888888',
    fontSize: 14,
    width: 65,
    fontWeight: 'bold',
  },
  dadosContaTexto: {
    color: '#000000',
    fontSize: 14,
  },
  valorSaqueContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    paddingTop: 0,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  valorSaqueLabel: {
    color: '#000000',
    fontSize: 14,
    width: 65,
    fontWeight: 'bold',
  },
  valorSaqueTexto: {
    color: '#000000',
    fontSize: 20,
  },
  atencaoTexto: {
    padding: 15,
    color: '#888888',
    fontSize: 14,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    textAlign: 'center',
  },
  btnContainer: {
    padding: 15,
    height: 140,
    justifyContent: 'space-between',
  },
});
