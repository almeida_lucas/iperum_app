/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

export default ModalSaqueAgendado = ({contaSelecionada, valorSaque}) => {
  const {nome, banco, agencia, conta, titular} = contaSelecionada;

  fechaModalSaqueAgendado = () => Navigation.dismissAllModals({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchOpctyPrevSelect onPress={fechaModalSaqueAgendado}>
          <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
        </TouchOpctyPrevSelect>
        <Text style={styles.contaSaqueTexto}>Saque agendado</Text>
      </View>
      <ScrollView>
        <View style={styles.containerDica}>
          <Text style={styles.textoDica}>Agora é só aguardar o valor chegar na sua conta:</Text>
          <TouchOpctyPrevSelect onPress={() => console.warn('TODO')}>
            <Image style={styles.imgDica} source={require('../resources/dica.png')}/>
          </TouchOpctyPrevSelect>
        </View>
        <View style={styles.dadosContaContainer}>
          <Text style={styles.nomeContaTexto}>{nome}</Text>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Banco:</Text>
            <Text style={styles.dadosContaTexto}>{banco}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Agência:</Text>
            <Text style={styles.dadosContaTexto}>{agencia}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Conta:</Text>
            <Text style={styles.dadosContaTexto}>{conta}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Titular:</Text>
            <Text style={styles.dadosContaTexto}>{titular}</Text>
          </View>
        </View>
        <View style={styles.valorSaqueContainer}>
          <Text style={styles.valorSaqueLabel}>Valor:</Text>
          <Text style={styles.valorSaqueTexto}>R$ {valorSaque}</Text>
        </View>
        <Text style={styles.atencaoTexto}>ATENÇÃO: O valor estará disponível na conta informada em até 2 dias
          úteis.</Text>
        <View style={styles.btnContainer}>
          <TouchOpctyPrevSelect
            onPress={fechaModalSaqueAgendado}>
            <Button
              title='OK'
              titleColor='#630851'
              backgroundColor='transparent'
              borderWidth={2}
              borderColor='#630851'
            />
          </TouchOpctyPrevSelect>
          {/*<TouchOpctyPrevSelect
            onPress={fechaModalSaqueAgendado}>
            <Button
              title='COMPARTILHAR'
              backgroundColor='#630851'
            />
          </TouchOpctyPrevSelect>*/}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  contaSaqueTexto: {
    marginLeft: 10,
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  containerDica: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#EEEEEE',
    padding: 15,
  },
  textoDica: {
    flex: 1,
    fontSize: 18,
    color: '#000000',
  },
  imgDica: {
    width: 25,
    height: 25,
  },
  nomeContaTexto: {
    fontSize: 16,
    color: '#000000',
    marginBottom: 10,
  },
  dadosContaContainer: {
    padding: 15,
    paddingBottom: 0,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  labelTextoContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  dadosContaLabel: {
    color: '#888888',
    fontSize: 14,
    width: 65,
    fontWeight: 'bold',
  },
  dadosContaTexto: {
    color: '#000000',
    fontSize: 14,
  },
  valorSaqueContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    paddingTop: 0,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  valorSaqueLabel: {
    color: '#000000',
    fontSize: 14,
    width: 65,
    fontWeight: 'bold',
  },
  valorSaqueTexto: {
    color: '#000000',
    fontSize: 20,
  },
  atencaoTexto: {
    padding: 15,
    color: '#888888',
    fontSize: 14,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  btnContainer: {
    padding: 15,
    height: 140,
    justifyContent: 'space-between',
  },
});
