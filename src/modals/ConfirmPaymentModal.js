import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { buscaCarteirasCliente } from '../actions/WalletAction';
import { realizaTransferencia } from '../actions/TokenTransfAction';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import ActivityIndicatorModal from '../modals/ActivityIndicatorModal';
import { Navigation } from 'react-native-navigation';
import { BUSCA_CLIENTE_ERRO, BUSCA_CLIENTE_SUCESSO, LONG, tokenExpirado } from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';
import { buscaCliente } from '../actions/ClientAction';

class ConfirmPaymentModal extends Component<{}> {
  state = {
    isConfirmDisabled: false,
    confirmBtPressed: false,
    interval: null,
  };

  closeConfirmPaymentModal = () => Navigation.dismissModal({
    animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
  });

  confirmarPagamento = () => {
    this.setState({confirmBtPressed: true});
    const {cliente, clienteSelecionado, valorPagamento, buscaCarteiras, carteiras, tokenTransf, finalizaTransferencia, limpaDadosPagamento, buscaCliente} = this.props;
    const {id, token} = cliente;
    limpaDadosPagamento();
    buscaCliente(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO)
          finalizaTransferencia(id, token, {
            token: tokenTransf,
            valor: valorPagamento,
            wid: carteiras[0].wid,
            id_destino: clienteSelecionado.id,
          }).then(res => {
            if (res.data.error) {
              ShowNativeMessage('Erro', res.data.error, LONG);
              this.closeConfirmPaymentModal();
            } else if (res.data.status) {
              Navigation.showModal({
                screen: 'ModalPagamentoSucesso',
                navigatorStyle: {
                  navBarHidden: true,
                },
                passProps: {
                  valorPagamento,
                  clienteSelecionado,
                  cliente,
                },
              });
              buscaCarteiras(id, token);
              this.setState({interval: setTimeout(() => this.setState({isConfirmDisabled: false}), 500)});
            }
          });
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(this.props.interval);
          tokenExpirado('Erro', error, LONG);
        }
      });
  };

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  disableConfirmPayment = () => {
    this.setState(
      {isConfirmDisabled: true},
      () => setTimeout(() => {
        if (this.state.confirmBtPressed) this.setState({confirmBtPressed: false});
        else this.setState({isConfirmDisabled: false});
      }, 500),
    );
  };

  render() {
    const {cliente, clienteSelecionado, valorPagamento} = this.props;
    const {isConfirmDisabled} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.closeConfirmPaymentModal}>
            <Image style={styles.imgFechaModal} source={require('../resources/setaEsquerda.png')}/>
          </TouchOpctyPrevSelect>
        </View>
        <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'stretch'}}>
          <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#AAAAAA'}}>Valor (R$)</Text>
            <Text style={{fontSize: 50, color: '#000000'}}>{valorPagamento}</Text>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              alignSelf: 'stretch',
              paddingLeft: 20,
              paddingRight: 20,
            }}>
              <View style={styles.usuarioContainer}>
                <View style={styles.iniciaisView}>
                  <Text style={styles.iniciais}>{cliente.iniciais}</Text>
                </View>
                <Text style={styles.titulo}>Você</Text>
                <Text style={styles.subTitulo}>{cliente.usuario}</Text>
              </View>
              <Image style={styles.imgFechaModal} source={require('../resources/seta_direita.png')}/>
              <View style={styles.usuarioContainer}>
                <View style={styles.iniciaisView}>
                  <Text style={styles.iniciais}>{clienteSelecionado.iniciais}</Text>
                </View>
                <Text style={styles.titulo}>{clienteSelecionado.nome}</Text>
                <Text style={styles.subTitulo}>{clienteSelecionado.usuario}</Text>
              </View>
            </View>
          </View>
          <View style={styles.pagamentoFooter}>
            <Text style={styles.pagamentoConfirmacaoTexto}>Deseja confirmar esse pagamento?</Text>
            <TouchOpctyPrevSelect
              onPressOut={this.disableConfirmPayment}
              onPress={this.confirmarPagamento}
              disabled={isConfirmDisabled}>
              <Button
                title='CONFIRMAR PAGAMENTO'
                titleColor='#FFFFFF'
                backgroundColor={'#ffbd01'}
              />
            </TouchOpctyPrevSelect>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({cliente, tokenTransf, interval}) => {
  return {
    cliente,
    tokenTransf,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    buscaCarteiras: (id, token) => dispatch(buscaCarteirasCliente(id, token)),
    finalizaTransferencia: (id, token, data) => dispatch(realizaTransferencia(id, token, data)),
    buscaCliente: (id, token) => dispatch(buscaCliente(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmPaymentModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 15,
  },
  pagamentoFooter: {
    padding: 15,
    paddingTop: 0,
    paddingBottom: 30,
    alignItems: 'stretch',
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#efefef',
  },
  pagamentoConfirmacaoTexto: {
    fontSize: 15,
    color: '#000000',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 15,
  },
  usuarioContainer: {
    alignItems: 'center',
    width: 120,
    marginTop: 15,
  },
  iniciaisView: {
    width: 80,
    height: 80,
    borderRadius: 80,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 23,
    color: '#DDDDDD',
  },
  titulo: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center',
  },
  subTitulo: {
    fontSize: 14,
    color: '#AAAAAA',
  },
});
