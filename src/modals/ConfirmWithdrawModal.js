/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image, Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { novaSolicitacao, buscaSolicitacoes } from '../actions/AccountAction';
import { buscaCarteirasCliente } from '../actions/WalletAction';

import { Navigation } from 'react-native-navigation';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import { ShowNativeMessage } from '../components/NativeMessage';
import {
  BUSCA_CLIENTE_ERRO,
  BUSCA_CLIENTE_SUCESSO,
  LONG,
  NOVA_SOLICITACAO_ERRO,
  NOVA_SOLICITACAO_SUCESSO,
  SAQUE,
  tokenExpirado,
} from '../contants';
import { buscaCliente } from '../actions/ClientAction';

const ConfirmWithdrawModal = ({interval, contaSelecionada, valorSaque, cliente, novaSolicit, buscaSolicit, buscaCart, buscaCli}) => {
  const {nome, banco, agencia, conta, titular} = contaSelecionada;
  const {documento, id, token} = cliente;

  const fechaModalConfirmaSaque = () => Navigation.dismissModal({
    animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
  });

  const saqueSucesso = () => {
    Navigation.showModal({
      screen: 'ModalSaqueAgendado',
      navigatorStyle: {
        navBarHidden: true,
      },
      passProps: {
        contaSelecionada,
        valorSaque,
      },
    });
    buscaSolicit(id, token);
    buscaCart(id, token);
  };

  const saqueErro = (msgErro) => {
    ShowNativeMessage('Saque', msgErro, LONG);
  };

  const confirmaSaque = () => {
    const dadosSolicitacao = {
      nome_conta: nome,
      valor: valorSaque,
      titular,
      agencia,
      banco,
      conta,
      documento_titular: documento,
      tipo_solicitacao: SAQUE,
    };

    buscaCli(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO)
          novaSolicit(dadosSolicitacao, id, token)
            .then(({type, error}) => {
              if (type === NOVA_SOLICITACAO_SUCESSO) saqueSucesso();
              else if (type === NOVA_SOLICITACAO_ERRO) saqueErro(error);
            });
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(interval);
          tokenExpirado('Erro', error, LONG);
        }
      });
  };

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={fechaModalConfirmaSaque}>
            <Image style={styles.imgFechaModal} source={require('../resources/setaEsquerda.png')}/>
          </TouchOpctyPrevSelect>
        </View>
        <Text style={styles.tituloConfirmaSaqueTexto}>O valor será enviado para a seguinte conta:</Text>
        <View style={styles.dadosContaContainer}>
          <Text style={styles.nomeContaTexto}>{nome}</Text>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Banco:</Text>
            <Text style={styles.dadosContaTexto}>{banco}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Agência:</Text>
            <Text style={styles.dadosContaTexto}>{agencia}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Conta:</Text>
            <Text style={styles.dadosContaTexto}>{conta}</Text>
          </View>
          <View style={styles.labelTextoContainer}>
            <Text style={styles.dadosContaLabel}>Titular:</Text>
            <Text style={styles.dadosContaTexto}>{titular}</Text>
          </View>
        </View>
        <View style={styles.valorSaqueContainer}>
          <Text style={styles.valorSaqueLabel}>Valor:</Text>
          <Text style={styles.valorSaqueTexto}>R$ {valorSaque}</Text>
        </View>
        <Text style={styles.atencaoTexto}>ATENÇÃO: O valor estará disponível na conta informada em até 2 dias
          úteis.</Text>
      </View>
      <View style={styles.saqueFooter}>
        <Text style={styles.saqueConfirmacaoTexto}>Deseja confirmar o saque?</Text>
        <TouchOpctyPrevSelect
          onPress={confirmaSaque}>
          <Button
            title='CONFIRMAR O SAQUE'
            titleColor='#FFFFFF'
            backgroundColor={'#ffbd01'}
          />
        </TouchOpctyPrevSelect>
      </View>
    </View>
  );
};

const mapStateToProps = ({cliente, interval}) => {
  return {
    cliente,
    interval,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    novaSolicit: (data, id, token) => dispatch(novaSolicitacao(data, id, token)),
    buscaSolicit: (id, token) => dispatch(buscaSolicitacoes(id, token)),
    buscaCart: (id, token) => dispatch(buscaCarteirasCliente(id, token)),
    buscaCli: (id, token) => dispatch(buscaCliente(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmWithdrawModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    paddingBottom: 10,
    alignItems: 'center',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  tituloConfirmaSaqueTexto: {
    fontSize: 18,
    color: '#000000',
    padding: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#EEEEEE',
  },
  nomeContaTexto: {
    fontSize: 16,
    color: '#000000',
    marginBottom: 10,
  },
  dadosContaContainer: {
    padding: 15,
    paddingBottom: 0,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  labelTextoContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  dadosContaLabel: {
    color: '#888888',
    fontSize: 14,
    width: 80,
    fontWeight: 'bold',
  },
  dadosContaTexto: {
    color: '#000000',
    fontSize: 14,
  },
  valorSaqueContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    paddingTop: 0,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  valorSaqueLabel: {
    color: '#000000',
    fontSize: 14,
    width: 80,
    fontWeight: 'bold',
  },
  valorSaqueTexto: {
    color: '#000000',
    fontSize: 20,
  },
  atencaoTexto: {
    padding: 15,
    color: '#888888',
    fontSize: 14,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  saqueFooter: {
    padding: 15,
    paddingTop: 0,
    paddingBottom: 30,
    alignItems: 'stretch',
  },
  saqueConfirmacaoTexto: {
    fontSize: 15,
    color: '#000000',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },
});
