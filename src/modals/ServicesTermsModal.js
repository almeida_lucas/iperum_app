import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  ScrollView,
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

const text = 'Esse site é operado pelo Iperum. Em todo o site, os termos “nós”, “nos” e “nosso” se referem ao ' +
  'Iperum. O Iperum proporciona esse site, incluindo todas as informações, ferramentas e serviços ' +
  'disponíveis deste site para você, o usuário, com a condição da sua aceitação de todos os termos, ' +
  'condições, políticas e avisos declarados aqui.\n\n' +
  'Ao visitar nosso site e/ou comprar alguma coisa no nosso site, você está utilizando nossos “Serviços”. ' +
  'Consequentemente, você concorda com os seguintes termos e condições (“Termos de serviço”, ' +
  '“Termos”), incluindo os termos e condições e políticas adicionais mencionados neste documento e/ou ' +
  'disponíveis por hyperlink. Esses Termos de serviço se aplicam a todos os usuários do site, incluindo, sem ' +
  'limitação, os usuários que são navegadores, fornecedores, clientes, lojistas e/ou contribuidores de ' +
  'conteúdo.\n\n' +
  'Por favor, leia esses Termos de serviço cuidadosamente antes de acessar ou utilizar o nosso site. Ao ' +
  'acessar ou usar qualquer parte do site, você concorda com os Termos de serviço. Se você não concorda ' +
  'com todos os termos e condições desse acordo, então você não pode acessar o site ou usar quaisquer ' +
  'serviços. Se esses Termos de serviço são considerados uma oferta, a aceitação é expressamente limitada ' +
  'a esses Termos de serviço.\n\n' +
  'Quaisquer novos recursos ou ferramentas que forem adicionados à loja atual também devem estar ' +
  'sujeitos aos Termos de serviço.Você pode revisar a versão mais atual dos Termos de serviço quando ' +
  'quiser nesta página. Reservamos o direito de atualizar, alterar ou trocar qualquer parte desses Termos ' +
  'de serviço ao publicar atualizações e/ou alterações no nosso site. É sua responsabilidade verificar as ' +
  'alterações feitas nesta página periodicamente. Seu uso contínuo ou acesso ao site após a publicação de ' +
  'quaisquer alterações constitui aceitação de tais alterações.';

export default ModalTermosServicos = () => {
  const fechaModalTermosServicos = () => {
    Navigation.dismissModal({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  };

  return (
    <ScrollView style={{flex: 1,}} contentContainerStyle={styles.container}>
      <View style={styles.headerContainer}>
        <TouchOpctyPrevSelect onPress={fechaModalTermosServicos}>
          <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
        </TouchOpctyPrevSelect>
      </View>
      <View style={{padding: 10,}}>
        <Text style={styles.titulo}>Termos de serviços</Text>
        <Text style={styles.subTitulo}>Visão geral</Text>
        <Text style={styles.texto}>{text}</Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
    padding: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 15,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  titulo: {
    marginBottom: 10,
    fontSize: 26,
    color: '#000000',
  },
  subTitulo: {
    marginBottom: 5,
    fontSize: 18,
    color: '#AAAAAA',
  },
  texto: {
    fontSize: 14,
  },
});