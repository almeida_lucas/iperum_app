/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text, Platform,
} from 'react-native';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import { TextInputMask } from 'react-native-masked-text';
import { Navigation } from 'react-native-navigation';

// type Props = {};

export default class WithdrawModal extends Component<{}> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  state = {
    valorSaque: '0,00',
    contaSelecionada: null,
  };

  componentDidMount() {

  }

  selecionaContaSaque = (contaSelecionada) => this.setState({contaSelecionada});

  sacar = () => {
    const {contaSelecionada, valorSaque} = this.state;
    this.props.navigator.showModal({
      screen: 'ModalConfirmaSaque',
      navigatorStyle: {
        navBarHidden: true,
      },
      passProps: {
        contaSelecionada,
        valorSaque,
      },
    });
  };

  mostraEnviarParaModal = () => this.props.navigator.showModal({
    screen: 'ModalEnviarPara',
    navigatorStyle: {
      navBarHidden: true,
    },
    passProps: {
      selecionaContaSaque: this.selecionaContaSaque,
    },
  });

  fechaModalSaque = () => Navigation.dismissModal({
    animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
  });

  onChangeText = (valorSaque) => this.setState({valorSaque});

  render() {
    const {valorSaque, contaSelecionada} = this.state;
    const valorSaqueValido = valorSaque !== '0,00';

    return (
      <View style={styles.container}>
        <View>
          <View style={styles.headerContainer}>
            <TouchOpctyPrevSelect onPress={this.fechaModalSaque}>
              <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
            </TouchOpctyPrevSelect>
            <Text style={styles.contaSaqueTexto}>Conta para saque</Text>
          </View>
          <View style={styles.containerValorSaque}>
            <Text>Valor do Saque (R$)</Text>
            <TextInputMask
              type='money'
              value={valorSaque}
              onChangeText={this.onChangeText}
              options={{unit: ''}}
              underlineColorAndroid="transparent"
              style={{alignSelf: 'stretch', textAlign: 'right', color: '#AAAAAA', fontSize: 40}}
              maxLength={9}
              returnKeyType='done'
            />
          </View>
          <TouchOpctyPrevSelect onPress={this.mostraEnviarParaModal}>
            <View style={styles.enviarParaContainer}>
              <Text style={styles.enviarParaTexto}>Enviar para:</Text>
              <Text style={styles.contaSelecionada}>{contaSelecionada ? contaSelecionada.nome : ''}</Text>
            </View>
          </TouchOpctyPrevSelect>
        </View>
        <View style={styles.footer}>
          <TouchOpctyPrevSelect
            onPress={this.sacar}
            disabled={!valorSaqueValido || contaSelecionada == null}>
            <Button
              title='SACAR'
              backgroundColor='#ffc210'
              titleSize={20}
              opacity={valorSaqueValido && contaSelecionada != null ? 1 : 0.8}
            />
          </TouchOpctyPrevSelect>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 0,
    paddingTop: Platform.OS === 'ios' ? 15 : 0,
    backgroundColor: '#FFFFFF',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  contaSaqueTexto: {
    marginLeft: 10,
    fontSize: 14,
    color: '#000000',
    fontWeight: 'bold',
  },
  saqueContainer: {
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
  },
  containerValorSaque: {
    padding: 15,
    paddingBottom: 0,
  },
  saqueTexto: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  valorSaqueContainer: {
    alignItems: 'stretch',
  },
  saqueValor: {
    fontSize: 60,
    color: '#DDDDDD',
    textAlign: 'right',
  },
  enviarParaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
    padding: 15,
  },
  enviarParaTexto: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  contaSelecionada: {
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  footer: {
    padding: 15,
  },
});
