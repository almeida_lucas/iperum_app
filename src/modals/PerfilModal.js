/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  AsyncStorage,
  Platform,
} from 'react-native';

import { Navigation } from 'react-native-navigation';

import PerfilData from '../components/PerfilData';
import { connect } from 'react-redux';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

const PerfilModal = ({cliente, contas, interval}) => {
  const {nome, iniciais, documento, email, usuario, telefone} = cliente;
  const contaPrincipal = contas.filter((conta) => conta.conta_principal);

  const fechaModalPerfil = () => {
    Navigation.dismissModal({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  };

  sair = () => {
    // Navigation.dismissModal({
    //   animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    // });
    AsyncStorage.clear();

    clearInterval(interval);

    Navigation.startSingleScreenApp({
      screen: {
        screen: 'SplashScreen',
      },
    });
  };

  const mostraModalCadastraContaSaque = () => Navigation.showModal({
    screen: 'ModalCadastraContaSaque',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const mostraModalTermosServico = () => Navigation.showModal({
    screen: 'ModalTermosServicos',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const mostraModalPoliticaPrivacidade = () => Navigation.showModal({
    screen: 'ModalPoliticaPrivacidade',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const mudaModalEditaPerfil = () => Navigation.showModal({
    screen: 'ModalEditaPerfil',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const mudaModalEditaSenha = () => Navigation.showModal({
    screen: 'EditPassword',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={styles.subHeaderContainer}>
          <TouchOpctyPrevSelect onPress={fechaModalPerfil}>
            <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
          </TouchOpctyPrevSelect>
          <Text style={styles.perfilTexto}>Perfil</Text>
        </View>
        <TouchOpctyPrevSelect onPress={mudaModalEditaPerfil}>
          <Image style={styles.imgEdita} source={require('../resources/engrenagem.png')}/>
        </TouchOpctyPrevSelect>
      </View>
      <ScrollView>
        <View style={styles.avatarContainer}>
          <View style={styles.avatarConteudo}>
            <View style={styles.iniciaisView}>
              <Text style={styles.iniciais}>{iniciais}</Text>
            </View>
          </View>
          <Text style={styles.dadosPessoaisTexto}>Dados pessoais</Text>
        </View>
        <PerfilData label='Nome Completo' value={nome}/>
        <PerfilData label='CPF' value={documento}/>
        <PerfilData label='E-mail' value={email}/>
        <PerfilData label='Nome de usuário (apelido)' value={usuario}/>
        <PerfilData label='Celular' value={telefone}/>
        <View style={styles.containerAlterarSenha}>
          <TouchOpctyPrevSelect onPress={mudaModalEditaSenha}>
            <Text style={styles.textoAlterarSenha}>Alterar Senha</Text>
          </TouchOpctyPrevSelect>
        </View>
        <Text style={styles.dadosBancariosTexto}>Dados bancários</Text>
        <View style={styles.listaContas}>
          <Text
            style={styles.contaSelecionada}>{contaPrincipal.length > 0 ? contaPrincipal[0].nome : 'Cadastre uma nova conta'}</Text>
          <Text style={styles.contaPrincipal}>{contaPrincipal.length > 0 ? '(Principal)' : ''}</Text>
        </View>
        <TouchOpctyPrevSelect onPress={mostraModalCadastraContaSaque}>
          <View style={styles.containerAdicionarConta}>
            <Image style={styles.imgMais} source={require('../resources/mais.png')}/>
            <Text style={styles.adicionaConta}>Adicionar outra conta</Text>
          </View>
        </TouchOpctyPrevSelect>

        <View style={styles.containerTermosPolitica}>
          <TouchOpctyPrevSelect onPress={mostraModalTermosServico}>
            <View style={styles.itemTermos}>
              <Text style={styles.textoTermosPolitica}>Termos de serviços</Text>
            </View>
          </TouchOpctyPrevSelect>
          <TouchOpctyPrevSelect onPress={mostraModalPoliticaPrivacidade}>
            <View style={styles.itemPolitica}>
              <Text style={styles.textoTermosPolitica}>Política de privacidade</Text>
            </View>
          </TouchOpctyPrevSelect>
        </View>
        <TouchOpctyPrevSelect onPress={sair}>
          <View style={styles.containerSair}>
            <Text style={styles.textoSair}>Sair</Text>
          </View>
        </TouchOpctyPrevSelect>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = ({cliente, conta, interval}) => {
  const {contas} = conta;
  return {
    cliente,
    contas,
    interval,
  };
};

export default connect(mapStateToProps, null)(PerfilModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#eff2f3',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  subHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  imgEdita: {
    width: 25,
    height: 25,
    margin: 5,
  },
  perfilTexto: {
    marginLeft: 10,
    fontSize: 18,
    color: '#000000',
    fontWeight: 'bold',
  },
  avatarContainer: {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  avatarConteudo: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
  iniciaisView: {
    width: 100,
    height: 100,
    borderRadius: 100,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 38,
    color: '#AAAAAA',
  },
  dadosPessoaisTexto: {
    marginLeft: 15,
    marginBottom: 5,
    fontSize: 16,
    color: '#888888',
  },
  containerAlterarSenha: {
    marginTop: 15,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  textoAlterarSenha: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#862777',
  },
  dadosBancariosTexto: {
    marginLeft: 15,
    marginTop: 15,
    marginBottom: 5,
    fontSize: 16,
    color: '#888888',
  },

  listaContas: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  contaSelecionada: {
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  contaPrincipal: {
    fontSize: 16,
    color: '#BBBBBB',
  },
  containerAdicionarConta: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  imgMais: {
    width: 20,
    height: 20,
  },
  adicionaConta: {
    fontSize: 16,
    color: '#630851',
    marginLeft: 10,
  },

  containerTermosPolitica: {
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#FFFFFF',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  itemTermos: {
    padding: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  itemPolitica: {
    padding: 15,
  },
  textoTermosPolitica: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#862777',
  },
  containerSair: {
    padding: 15,
    marginBottom: 30,
    backgroundColor: '#FFFFFF',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  textoSair: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'red',
  },
});
