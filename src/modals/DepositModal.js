/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Platform,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import { connect } from 'react-redux';

import { Navigation } from 'react-native-navigation';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import { ShowNativeMessage } from '../components/NativeMessage';
import {
  BUSCA_CLIENTE_ERRO,
  BUSCA_CLIENTE_SUCESSO,
  DEPOSITO,
  LONG,
  NOVA_SOLICITACAO_ERRO,
  NOVA_SOLICITACAO_SUCESSO,
  tokenExpirado,
} from '../contants';
import ActivityIndicatorModal from '../modals/ActivityIndicatorModal';
import { buscaSolicitacoes, novaSolicitacao } from '../actions/AccountAction';
import { buscaExtrato } from '../actions/ExtractAction';
import { buscaCliente } from '../actions/ClientAction';

class DepositModal extends Component<{}> {

  state = {
    valorDeposito: '23,00',
    isConfirmDisabled: false,
    confirmBtPressed: false,
    interval: null,
  };

  fechaModalDeposito = () => Navigation.dismissModal(this.props.componentId);

  depositoSucesso = () => Navigation.showModal({
    component: {
      id: 'ConfirmDepositModal',
      name: 'ConfirmDepositModal',
      passProps: {
        valorDeposito: this.state.valorDeposito,
      },
      options: {
        screenBackgroundColor: 'transparent',
        modalPresentationStyle: 'overCurrentContext',
      },
    },
  });

  depositoErro = (msgErro) => ShowNativeMessage('Deposito', msgErro, LONG);

  confirmDepositModal = () => {
    this.setState({confirmBtPressed: true});

    const {nome, documento, id, token} = this.props.cliente;

    const dadosSolicitacao = {
      valor: this.state.valorDeposito,
      titular: nome,
      documento_titular: documento,
      tipo_solicitacao: DEPOSITO,
    };

    this.props.buscaCliente(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO)
          this.props.novaSolicitacao(dadosSolicitacao, id, token)
            .then(({type, error}) => {
              if (type === NOVA_SOLICITACAO_SUCESSO) this.depositoSucesso();
              else if (type === NOVA_SOLICITACAO_ERRO) this.depositoErro(error);
              this.setState({interval: setTimeout(() => this.setState({isConfirmDisabled: false}), 500)});
            });
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(this.props.interval);
          tokenExpirado('Erro', error, LONG);
        }
      });
  };

  onChangeText = (valorDeposito) => this.setState({valorDeposito});

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  disableConfirm = () => {
    this.setState(
      {isConfirmDisabled: true},
      () => setTimeout(() => {
        if (this.state.confirmBtPressed) this.setState({confirmBtPressed: false});
        else this.setState({isConfirmDisabled: false});
      }, 500),
    );
  };

  render() {
    const {valorDeposito, isConfirmDisabled} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.fechaModalDeposito}>
            <Image style={styles.imgFechaModal} source={require('../resources/setaEsquerda.png')}/>
          </TouchOpctyPrevSelect>
          <Text style={styles.depositoTexto}>Depósito</Text>
        </View>
        <View style={styles.containerPrincipal}>
          <View style={styles.containerValorDeposito}>
            <Text>Valor do depósito (R$)</Text>
            <TextInputMask
              type='money'
              value={valorDeposito}
              onChangeText={this.onChangeText}
              options={{unit: ''}}
              underlineColorAndroid="transparent"
              style={{alignSelf: 'stretch', textAlign: 'right', color: '#AAAAAA', fontSize: 40}}
              maxLength={9}
              returnKeyType='done'
            />
          </View>
          <View style={styles.depositoFooter}>
            <Text style={styles.depositoConfirmacaoTexto}>Deseja confirmar esse depósito?</Text>
            <TouchOpctyPrevSelect
              onPressOut={this.disableConfirm}
              onPress={this.depositoSucesso}
              disabled={valorDeposito === '0,00' || isConfirmDisabled}>
              <Button
                title='CONFIRMAR DEPÓSITO'
                titleColor='#FFFFFF'
                backgroundColor={valorDeposito !== '0,00' ? '#ffbd01' : '#ffef56'}
              />
            </TouchOpctyPrevSelect>
          </View>
        </View>
        {/*<ActivityIndicatorModal visible={isConfirmDisabled}/>*/}
      </View>
    );
  }
}

const mapStateToProps = ({cliente, interval}) => {
  return {
    cliente,
    interval
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    novaSolicitacao: (data, id, token) => dispatch(novaSolicitacao(data, id, token)),
    buscaSolicitacoes: (id, token) => dispatch(buscaSolicitacoes(id, token)),
    buscaTransacoes: (id, token) => dispatch(buscaExtrato(id, token)),
    buscaCliente: (id, token) => dispatch(buscaCliente(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DepositModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
    paddingTop: Platform.OS === 'ios' ? 15 : 0,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  depositoTexto: {
    marginLeft: 10,
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  containerPrincipal: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  containerValorDeposito: {
    padding: 15,
    paddingBottom: 0,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  depositoFooter: {
    padding: 15,
    paddingBottom: 30,
    alignItems: 'stretch',
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
  },
  depositoConfirmacaoTexto: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 20,
    marginTop: 5,
  },
});
