import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  ScrollView,
  Platform,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

const text = 'Com um mundo cada vez mais digital e globalizado, as pessoas tem ficado cada vez ' +
  'mais expostas, comparilhando mesmo que sem perceber informações muito sensíveis, ' +
  'como por exemplo, senhas de bancos e sites, documentos privados, etc.\n\n' +
  'Pensando nisso, nós da Iperum desenvolvemos uma política de privacidade simples ' +
  'mas super clara e objetiva para garantir que tanto nós quanto os usuários estão cientes ' +
  'do uso/compartilhamento das informações e o que isso significa para todos\n\n' +
  'Coleta e armazenamento de dados: todos os dados fornececidos pelos usuários são ' +
  'armazenados com criptografia de dados. A criptografia é utilizada também durante as ' +
  'transações para que seus dados não fiquem expostos à terceiros. Jamais solicitamos ' +
  'informações sensíveis através de outros canais que não seja o aplicativo. Informções ' +
  'não tão pessoais, como por exemplo, pesquisas de habitos de consumo, ' +
  'escolariedade, ocupação, podem ser coletadas por outros canais, mas os usuários ' +
  'devem ficar atentos e qualuquer desconfiancá jamais compartilhar dados pessoais;\n\n' +
  'Utilização de dados: seus dados são utilizados por nós para que possamos aprimorar ' +
  'nossos serviços e oferecer aos clientes melhores produtos e experiência. Ao se ' +
  'cadastrar e fornecer número de telefone, email o cliente está autorizando a Iperum ' +
  'utilizar esses dados para qualquer tipo de comunicação, e em determinados casos ' +
  'utilizar-los com parceiros para aprimorar ainda mais nossos serviços;\n\n' +
  'Compartilhamento de dado: alguns dados são compartilhados automaticamente com ' +
  'outros usuários da plataforma com o propósito exclusivo do funcionamento da mesma, ' +
  'por exemplo seu usuário sem o qual a plataforma não teria como operar.\n\n' +
  'Dúvidas? Não hesite, entre em contato conosco que lhe daremos todo suporte em caso ' +
  'de dúvidas sobre utilização de suas informações. Mande um e-mail para ' +
  'socorro@iperum.com.br.\n\n' +
  'Essa política está sujeita a alterações e é importante que o usuário esteja sempre ' +
  'atento à quaisquer alterações da mesma.';

export default ModalPoliticaPrivacidade = () => {
  const fechaModalPoliticaPrivacidade = () => {
    Navigation.dismissModal({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.headerContainer}>
        <TouchOpctyPrevSelect onPress={fechaModalPoliticaPrivacidade}>
          <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
        </TouchOpctyPrevSelect>
      </View>
      <View style={{padding: 10}}>
        <Text style={styles.titulo}>Política de privacidade</Text>
        <Text style={styles.subTitulo}>Visão geral</Text>
        <Text style={styles.texto}>{text}</Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
    padding: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 15,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
  },
  titulo: {
    marginBottom: 10,
    fontSize: 26,
    color: '#000000',
  },
  subTitulo: {
    marginBottom: 5,
    fontSize: 18,
    color: '#AAAAAA',
  },
  texto: {
    fontSize: 14,
  },
});