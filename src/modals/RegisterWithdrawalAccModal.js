/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TextInput,
  Picker,
  KeyboardAvoidingView,
  ScrollView, Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { cadastraConta, buscaContasCadastradas } from '../actions/AccountAction';

import { Navigation } from 'react-native-navigation';
import Button from '../components/Button';
import { ShowNativeMessage } from '../components/NativeMessage';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import { TextInputMask } from 'react-native-masked-text';
import { LONG } from '../contants';

const width = Dimensions.get('screen').width;

class RegisterWithdrawalAccModal extends Component<{}> {

  state = {
    banco: {
      text: 'Selecione um banco...',
      value: '',
    },
    acc_type: {
      text: 'Selecione o tipo de conta...',
      value: null,
    },
    nomeConta: '',
    conta: '',
    digito_conta: '',
    agencia: '',
    digito_agencia: '',
  };

  fechaModalCadastraContaSaque = () => {
    Navigation.dismissModal({
      animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  };

  cadastroSucesso = () => {
    const {id, token} = this.props.cliente;
    this.props.buscaContasCadastradas(id, token);
    this.fechaModalCadastraContaSaque();
  };

  salvarConta = () => {
    const {nome, documento, id, token} = this.props.cliente;
    const {nomeConta, banco, conta, digito_conta, agencia, digito_agencia, acc_type} = this.state;
    const novaConta = {
      nome: nomeConta,
      banco: banco.value,
      conta,
      digito_conta,
      agencia,
      digito_agencia,
      conta_principal: acc_type.value,
      titular: nome,
      documento_titular: documento,
    };

    if (novaConta.nome && novaConta.nome !== '' && novaConta.banco && novaConta.banco !== ''
      && novaConta.conta && novaConta.conta !== '' && novaConta.digito_conta && novaConta.digito_conta !== ''
      && novaConta.agencia && novaConta.digito_agencia !== '' && novaConta.titular && novaConta.titular !== ''
      && novaConta.documento_titular && novaConta.documento_titular !== '' && novaConta.conta_principal !== null) {
      this.props.cadastraConta(novaConta, id, token, this.cadastroSucesso);
    } else {
      ShowNativeMessage('Cadastro conta', 'Preencha todos os campos corretamente!', LONG);
    }
  };

  onChangeBanco = (banco, itemIndex) => this.setState({banco});
  onChangeTipoConta = (conta_principal, itemIndex) => this.setState({conta_principal});
  onChangeNomeConta = (nomeConta) => this.setState({nomeConta});
  onChangeConta = (conta) => this.setState({conta});
  onChangeDigitoConta = (digito_conta) => this.setState({digito_conta});
  onChangeAgencia = (agencia) => this.setState({agencia});
  onChangeDigitoAgencia = (digito_agencia) => this.setState({digito_agencia});

  selecionaContaBanco = (banco) => this.setState({banco});

  showBankList = () => Navigation.showModal({
    screen: 'SelectListModal',
    navigatorStyle: {
      navBarHidden: true,
    },
    passProps: {
      itens: [
        {text: '341 - Itau', value: '341'},
        {text: '237 - Bradesco', value: '237'},
        {text: '033 - Santander', value: '033'},
        {text: '001 - Banco do Brasil', value: '001'},
        {text: '104 - Caixa', value: '104'},
      ],
      selecionaItem: this.selecionaContaBanco,
    },
  });

  selecionaAccType = (acc_type) => this.setState({acc_type});

  showTypeAccList = () => Navigation.showModal({
    screen: 'SelectListModal',
    navigatorStyle: {
      navBarHidden: true,
    },
    passProps: {
      itens: [
        {text: 'Conta Principal', value: true},
        {text: 'Conta Secundária', value: false},
      ],
      selecionaItem: this.selecionaAccType,
    },
  });

  render() {
    const {banco, acc_type, nomeConta, conta, digito_conta, agencia, digito_agencia} = this.state;

    return (
      <KeyboardAvoidingView style={styles.container} enabled={true}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.fechaModalCadastraContaSaque}>
            <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
          </TouchOpctyPrevSelect>
          <Text style={styles.contaSaqueTexto}>Conta para saque</Text>
        </View>
        <ScrollView>
          <View style={styles.containerDica}>
            <Text style={styles.textoDica}>Preencha os campos com os seus dados bancários</Text>
          </View>
          <View style={styles.dadosContaContainer}>
            <View style={styles.containerTextInputs}>
              <TouchOpctyPrevSelect onPress={this.showBankList}>
                <View style={styles.selectMenuView}>
                  <Text style={styles.selectMenu}>
                    {banco.text}
                  </Text>
                </View>
              </TouchOpctyPrevSelect>
              <View style={styles.containerInputs}>
                <TextInputMask
                  style={styles.textInputPrincipal}
                  type='only-numbers'
                  placeholder='Agência'
                  value={agencia}
                  onChangeText={this.onChangeAgencia}
                  underlineColorAndroid="transparent"
                  returnKeyType='done'
                />
                <TextInputMask
                  style={styles.textInputSecundario}
                  type='only-numbers'
                  placeholder='Digito'
                  value={digito_agencia}
                  onChangeText={this.onChangeDigitoAgencia}
                  underlineColorAndroid="transparent"
                  returnKeyType='done'
                />
              </View>
              <View style={styles.containerInputs}>
                <TextInputMask
                  style={styles.textInputPrincipal}
                  type='only-numbers'
                  placeholder='Conta'
                  value={conta}
                  onChangeText={this.onChangeConta}
                  underlineColorAndroid="transparent"
                  returnKeyType='done'
                />
                <TextInputMask
                  style={styles.textInputSecundario}
                  type='only-numbers'
                  placeholder='Digito'
                  value={digito_conta}
                  onChangeText={this.onChangeDigitoConta}
                  underlineColorAndroid="transparent"
                  returnKeyType='done'
                />
              </View>
              <TouchOpctyPrevSelect onPress={this.showTypeAccList}>
                <View style={styles.selectMenuView}>
                  <Text style={styles.selectMenu}>
                    {acc_type.text}
                  </Text>
                </View>
              </TouchOpctyPrevSelect>
              <TextInput
                style={styles.textInput}
                underlineColorAndroid="transparent"
                value={nomeConta}
                onChangeText={this.onChangeNomeConta}
                placeholder="Nome da Conta"/>
            </View>
            <TouchOpctyPrevSelect
              onPress={this.salvarConta}>
              <Button
                title='SALVAR CONTA'
                titleColor='#FFFFFF'
                backgroundColor='#ffbd01'
              />
            </TouchOpctyPrevSelect>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = ({cliente}) => {
  return {
    cliente,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cadastraConta: (novaConta, id, token, callbackSucesso) => dispatch(cadastraConta(novaConta, id, token, callbackSucesso)),
    buscaContasCadastradas: (id, token) => dispatch(buscaContasCadastradas(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterWithdrawalAccModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    backgroundColor: '#FFFFFF',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  contaSaqueTexto: {
    marginLeft: 10,
    fontSize: 16,
    color: '#000000',
    fontWeight: 'bold',
  },
  containerDica: {
    padding: 15,
    flexDirection: 'row',
    backgroundColor: '#eff2f3',
  },
  imgDuvida: {
    width: 30,
    height: 30,
  },
  textoDica: {
    fontSize: 18,
    color: '#000000',
    width: width * 0.8,
  },
  dadosContaContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
    padding: 15,
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  containerTextInputs: {
    marginBottom: 15,
  },
  textInput: {
    fontSize: 14,
    borderColor: '#ffbd01',
    borderBottomWidth: 1,
    color: '#000000',
    height: 40,
  },
  containerInputs: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInputPrincipal: {
    flex: 1,
    borderColor: '#ffbd01',
    borderBottomWidth: 1,
    marginRight: 10,
    height: 40,
  },
  textInputSecundario: {
    width: 100,
    borderColor: '#ffbd01',
    borderBottomWidth: 1,
    height: 40,
  },
  selectMenuView: {
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#ffbd01',
    justifyContent: 'center',
  },
  selectMenu: {
    fontSize: 14,
    color: '#000',
  },
});