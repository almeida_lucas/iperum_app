/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
} from 'react-native';

import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { filtraClientes } from '../actions/ClientTransferAction';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import ViewShot from 'react-native-view-shot';
import Share from 'react-native-share';

class PaymentSuccessModal extends Component {

  fechaModalPagamentoSucesso = () => Navigation.dismissAllModals({
    animationType: 'slide-down', // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
  });

  shareReceipt = () => {
    const {cliente, clienteSelecionado, valorPagamento} = this.props;
    this.refs.receiptContent.capture().then(url => {
      console.warn('do something with ', url);
      const shareOptions = {
        title: 'Comprovante transferencia Iperum',//string
        subject: 'Comprovante transferencia Iperum',//string
        message: Platform.OS === 'ios' ? '' : `Transferencia de ${cliente.nome} para ${clienteSelecionado.nome}, no valor de R$ ${valorPagamento} realizada com sucesso.`,
        url,
      };
      Share.open(shareOptions)
        .then((res) => {
          console.warn(res);
        })
        .catch((err) => {
          err && console.warn(err);
        });
    });
  };

  getCurrentDate = () => {
    const dataAtual = new Date();
    const day = dataAtual.getDay() < 10 ? '0' + dataAtual.getDay().toString() : dataAtual.getDay().toString();
    const month = dataAtual.getMonth() + 1 < 10 ? '0' + (dataAtual.getMonth() + 1).toString() : (dataAtual.getMonth() + 1).toString();
    const year = dataAtual.getFullYear();
    const hour = dataAtual.getHours() < 10 ? '0' + dataAtual.getHours().toString() : dataAtual.getHours().toString();
    const minutes = dataAtual.getMinutes() < 10 ? '0' + dataAtual.getMinutes().toString() : dataAtual.getMinutes().toString();

    return `${day}/${month}/${year} às ${hour}:${minutes}`;
  };

  render() {
    const {cliente, clienteSelecionado, valorPagamento} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchOpctyPrevSelect onPress={this.fechaModalPagamentoSucesso}>
            <Image style={styles.imgFechaModal} source={require('../resources/fecha.png')}/>
          </TouchOpctyPrevSelect>
        </View>
        <ViewShot ref="receiptContent" options={{format: 'png',}}
                  style={{backgroundColor: '#FFFFFF'}}>
          <View style={{alignItems: 'center', marginTop: 15, marginBottom: 15,}}>
            <Image style={styles.imgSucesso} source={require('../resources/sucesso.png')}/>
            <Text style={{
              fontSize: 20,
              color: '#000000',
              fontWeight: 'bold',
              textAlign: 'center',
              width: 200,
              marginBottom: 10,
            }}>Pagamento realizado com sucesso!</Text>
            <Text style={{fontSize: 14, fontWeight: 'bold', color: '#AAAAAA'}}>Valor (R$)</Text>
            <Text style={{fontSize: 23, color: '#000000'}}>{valorPagamento}</Text>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              alignSelf: 'stretch',
              paddingLeft: 20,
              paddingRight: 20,
              marginBottom: 10,
            }}>
              <View style={styles.usuarioContainer}>
                <View style={styles.iniciaisView}>
                  <Text style={styles.iniciais}>{cliente.iniciais}</Text>
                </View>
                <Text style={styles.titulo}>Você</Text>
                <Text style={styles.subTitulo}>{cliente.usuario}</Text>
              </View>
              <Image style={styles.imgFechaModal} source={require('../resources/seta_direita.png')}/>
              <View style={styles.usuarioContainer}>
                <View style={styles.iniciaisView}>
                  <Text style={styles.iniciais}>{clienteSelecionado.iniciais}</Text>
                </View>
                <Text style={styles.titulo}>{clienteSelecionado.nome}</Text>
                <Text style={styles.subTitulo}>{clienteSelecionado.usuario}</Text>
              </View>
            </View>
            <Text style={{
              fontSize: 14,
              color: '#686868',
            }}>
              {this.getCurrentDate()}
            </Text>
          </View>
        </ViewShot>
        <View style={styles.btnContainer}>
          <TouchOpctyPrevSelect
            onPress={this.fechaModalPagamentoSucesso}>
            <Button
              title='OK'
              titleColor='#630851'
              backgroundColor='transparent'
              borderWidth={2}
              borderColor='#630851'
            />
          </TouchOpctyPrevSelect>
          <TouchOpctyPrevSelect
            onPress={this.shareReceipt}>
            <Button
              title='COMPARTILHAR'
              backgroundColor='#630851'
            />
          </TouchOpctyPrevSelect>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({listaClientes}) => {
  return {
    listaClientes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    filtraClientes: (listaClientes, filtro) => dispatch(filtraClientes(listaClientes, filtro)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSuccessModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    alignItems: 'stretch',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    alignItems: 'center',
  },
  imgFechaModal: {
    width: 20,
    height: 20,
    margin: 5,
    marginTop: 15,
  },
  imgSucesso: {
    width: 50,
    height: 50,
  },
  usuarioContainer: {
    alignItems: 'center',
    width: 120,
    marginTop: 15,
  },
  iniciaisView: {
    width: 80,
    height: 80,
    borderRadius: 80,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 23,
    color: '#DDDDDD',
  },
  titulo: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center',
  },
  subTitulo: {
    fontSize: 14,
    color: '#AAAAAA',
  },
  btnContainer: {
    padding: 15,
    height: 140,
    justifyContent: 'space-between',
  },
});
