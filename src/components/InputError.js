/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

export default InputError = ({msgErr, onChangeText, type, value, onBlur, placeholder, autoCapitalize, returnKeyLabel, maxLength, returnKeyType}) => {

  return (
    <View>
      {type ?
        <TextInputMask
          type={type}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          onBlur={onBlur}
          autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
          returnKeyLabel={returnKeyLabel ? returnKeyLabel : 'Ok'}
          style={styles.textInput}
          maxLength={maxLength ? maxLength : 23}
          underlineColorAndroid="transparent"
          returnKeyType={returnKeyType ? returnKeyType : 'done'}
        />
        :
        <TextInput
          style={styles.textInput}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          onBlur={onBlur}
          autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
          returnKeyLabel={returnKeyLabel ? returnKeyLabel : 'Ok'}
          maxLength={maxLength ? maxLength : 23}
          underlineColorAndroid="transparent"
          returnKeyType={returnKeyType ? returnKeyType : 'done'}
        />
      }

      {msgErr !== null && msgErr !== '' ?
        <Text style={styles.msgErro}>{msgErr}</Text>
        :
        null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  msgErro: {
    fontSize: 12,
    color: 'red',
    position: 'absolute',
    bottom: 0,
  },
  textInput: {
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#ffc210',
    marginBottom: 15,
  }
});
