import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default UserItem = ({nome, email, iniciais, usuario}) => {

  return (
    <View style={styles.usuarioContainer}>
      <View style={styles.iniciaisView}>
        <Text style={styles.iniciais}>{iniciais}</Text>
      </View>
      <View style={styles.tituloContainer}>
        <Text style={styles.titulo}>{nome}</Text>
        <Text style={styles.subTitulo}>{usuario}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  usuarioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 15,
    marginBottom: 0,
  },
  iniciaisView: {
    width: 40,
    height: 40,
    borderRadius: 40,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 16,
    color: '#DDDDDD',
  },
  tituloContainer: {
    justifyContent: 'flex-start',
    paddingLeft: 10,
  },
  titulo: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
  },
  subTitulo: {
    fontSize: 12,
    color: '#AAAAAA',
  },
});
