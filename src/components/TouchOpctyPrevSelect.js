/**
 * @flow
 */

import React, { Component } from 'react';
import {
  TouchableOpacity,
} from 'react-native';

export default class TouchOpctyPrevSelect extends Component<{}> {

  state = {
    isBtDisabled: false,
    interval: null,
  };

  componentWillUnmount() {
    clearTimeout(this.state.interval);
  }

  btPressOut = (onPressOut) => {
    this.setState({
      isBtDisabled: true,
      interval: setTimeout(() => this.setState({isBtDisabled: false}), 3000),
    });
    if (onPressOut) onPressOut();
  };

  render() {
    const {
      onPress,
      onPressOut,
      disabled,
    } = this.props;

    return (
      <TouchableOpacity onPressOut={() => this.btPressOut(onPressOut)} onPress={onPress}
                        disabled={disabled ? disabled : this.state.isBtDisabled}>
        {this.props.children}
      </TouchableOpacity>
    );
  }
};