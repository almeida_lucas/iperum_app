import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  Animated,
  Clipboard,
} from 'react-native';
import { ShowNativeMessage } from '../NativeMessage';
import { SHORT } from '../../contants';

const styles = StyleSheet.create({
  containerImgCloseDetails: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  imgCloseDetails: {
    width: 15,
    height: 15,
    margin: 10,
    marginRight: 15,
    marginBottom: 0,
  },
  animatedLine: {
    flexDirection: 'row',
    paddingLeft: 15,
    marginBottom: 10,
  },
  animatedLineLabel: {
    width: 70,
    color: '#888888',
    fontSize: 14,
  },
  animatedLineText: {
    color: '#000000',
    fontSize: 14,
  },
  copyLink: {
    color: '#630851',
    fontSize: 13,
    fontWeight: 'bold',
    marginLeft: 23,
  },
  copyLinkDisabled: {
    opacity: 0.6,
  },
  animatedFooter: {
    flexDirection: 'row',
    padding: 10,
    paddingLeft: 15,
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderColor: '#e0e0e0',
  },
  animatedFooterLabel: {
    width: 70,
    color: '#000000',
    fontSize: 14,
    fontWeight: 'bold',
  },
  animateFooterText: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

const AnmView = Animated.createAnimatedComponent(View);

export default class AnimatedExtractItem extends Component<{}> {

  constructor() {
    super();

    this.state = {
      anmViewHeight: new Animated.Value(0),
      iniciouAnimacao: false,
    };
    this.component = React.createRef();
  }

  toggleAnimation = () => {
    const {anmViewHeight, iniciouAnimacao} = this.state;

    if (iniciouAnimacao) this.setState({iniciouAnimacao: false}, () => Animated.timing(anmViewHeight, {toValue: 0}).start());
    else this.setState({iniciouAnimacao: true}, () => Animated.timing(anmViewHeight, {toValue: 220}).start());
  };

  endAnimation = () => {
    const {anmViewHeight} = this.state;

    this.setState({iniciouAnimacao: false}, () => Animated.timing(anmViewHeight, {toValue: 0}).start());
  };

  copyText = (text, type) => {
    const typeText = type === 'AG' ? 'Agencia' : 'Conta';
    ShowNativeMessage(null, `${typeText} ${text} copiada!`, SHORT);
    Clipboard.setString(text);
  };

  render() {
    const {tipo, valor, documento_titular, banco, agencia, conta, titular, tipo_cliente, children} = this.props;
    const {anmViewHeight, iniciouAnimacao} = this.state;

    return (
      <View>
        <TouchableOpacity onPress={this.toggleAnimation} disabled={tipo !== 'Depósito' && tipo !== 'Saque'}>
          {children}
        </TouchableOpacity>
        <AnmView style={{height: anmViewHeight}} ref={this.component}>
        {iniciouAnimacao ?
            <View>
            <View style={styles.containerImgCloseDetails}>
              <TouchableOpacity onPress={this.endAnimation}>
                <Image style={styles.imgCloseDetails} source={require('../../resources/fecha.png')}/>
              </TouchableOpacity>
            </View>
            <View style={styles.animatedLine}>
              <Text
                style={styles.animatedLineLabel}>{tipo_cliente === 'PJ' || tipo === 'Depósito' ? 'CNPJ' : 'CPF'}</Text>
              <Text
                style={styles.animatedLineText}>{tipo === 'Depósito' ? '30.400.143/00001-51' : documento_titular ? documento_titular : '-'}</Text>
            </View>
            <View style={styles.animatedLine}>
              <Text style={styles.animatedLineLabel}>Banco:</Text>
              <Text style={styles.animatedLineText}>{tipo === 'Depósito' ? 'Banco Itaú' : banco ? banco : '-'}</Text>
            </View>
            <View style={styles.animatedLine}>
              <Text style={styles.animatedLineLabel}>Agencia:</Text>
              <Text style={styles.animatedLineText}>{tipo === 'Depósito' ? '8552' : agencia ? agencia : '-'}</Text>
              <TouchableOpacity onPress={() => this.copyText(agencia, 'AG')}>
                <Text style={styles.copyLink}>COPIAR</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.animatedLine}>
              <Text style={styles.animatedLineLabel}>Conta:</Text>
              <Text style={styles.animatedLineText}>{tipo === 'Depósito' ? '24343-1' : conta ? conta : '-'}</Text>
              <TouchableOpacity onPress={() => this.copyText(conta, 'CC')}>
                <Text style={styles.copyLink}>COPIAR</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.animatedLine}>
              <Text style={styles.animatedLineLabel}>Titular:</Text>
              <Text
                style={styles.animatedLineText}>{tipo === 'Depósito' ? 'Iperum Serviços de Pagamentos' : titular ? titular : '-'}</Text>
            </View>
            <View style={styles.animatedFooter}>
              <Text style={styles.animatedFooterLabel}>Valor:</Text>
              <Text style={styles.animateFooterText}>R$ {valor}</Text>
            </View>
          </View>
          :
          null
        }
        </AnmView>
      </View>
    );
  }
}
