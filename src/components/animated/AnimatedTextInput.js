import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Animated,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

const styles = StyleSheet.create({
  container: {
    height: 40,
    padding: 0,
    margin: 0,
    marginBottom: 0,
  },
  icon: {
    paddingRight: 5,
    paddingLeft: 5,
    position: 'absolute',
    marginTop: 10,
  },
  label: {
    position: 'absolute',
  },
});

const AnmTextInput = Animated.createAnimatedComponent(TextInput);
const AnmText = Animated.createAnimatedComponent(Text);
const AnmTextInputMask = Animated.createAnimatedComponent(TextInputMask);

class AnimatedTextInput extends Component<{}> {

  startAnimation = () => {
    const {borderBottomWidth, fontSize, marginTop} = this.state;

    Animated.timing(borderBottomWidth, {toValue: 4}).start();
    Animated.timing(fontSize, {toValue: 14}).start();
    Animated.timing(marginTop, {toValue: -15}).start();
  };

  endAnimation = () => {
    const {borderBottomWidth, fontSize, marginTop} = this.state;
    const {value} = this.props;

    Animated.timing(borderBottomWidth, {toValue: 1}).start();
    if (value === '') {
      Animated.timing(fontSize, {toValue: 16}).start();
      Animated.timing(marginTop, {toValue: 10}).start();
    }
  };

  constructor() {
    super();
    this.state = {
      borderBottomWidth: new Animated.Value(1),
      fontSize: new Animated.Value(16),
      paddingLeft: new Animated.Value(0),
      marginTop: new Animated.Value(10),
    };

  }

  componentDidMount() {
    const {marginTop, fontSize} = this.state;
    const {value} = this.props;

    if (value !== '') {
      Animated.timing(marginTop, {toValue: -15}).start();
      Animated.timing(fontSize, {toValue: 14}).start();
    }
  }

  render() {
    const {style, labelFontColor, inputFontColor, label, borderColor, value, onChangeText, secureTextEntry, keyboardType, multiline, type, autoCapitalize} = this.props;
    const {fontSize, paddingLeft, marginTop, borderBottomWidth} = this.state;

    return (
      <View style={[{alignSelf: 'stretch'}, styles.container, style ? style : {}]}>
        <AnmText style={[
          styles.label,
          {
            color: labelFontColor ? labelFontColor : '#1e90ff',
            fontSize,
            paddingLeft,
            marginTop,
          }]}>
          {label ? label : ''}
        </AnmText>
        {type ?
          <AnmTextInputMask
            type={type}
            onFocus={this.startAnimation}
            onBlur={this.endAnimation}
            style={[
              {
                color: inputFontColor ? inputFontColor : '#000000',
                borderBottomWidth,
                fontSize: 16,
                borderColor: borderColor ? borderColor : '#1e90ff',
                height: 40,
              },
            ]}
            underlineColorAndroid='transparent'
            value={value}
            onChangeText={onChangeText}
            secureTextEntry={secureTextEntry ? secureTextEntry : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            multiline={multiline ? multiline : false}
            autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
          />
          :
          <AnmTextInput
            onFocus={this.startAnimation}
            onBlur={this.endAnimation}
            style={[
              {
                color: inputFontColor ? inputFontColor : '#000000',
                borderBottomWidth,
                fontSize: 16,
                borderColor: borderColor ? borderColor : '#1e90ff',
                height: 40,
              },
            ]}
            underlineColorAndroid='transparent'
            value={value}
            onChangeText={onChangeText}
            secureTextEntry={secureTextEntry ? secureTextEntry : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            multiline={multiline ? multiline : false}
            autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
          />
        }
      </View>
    );
  }
}

export default AnimatedTextInput;
