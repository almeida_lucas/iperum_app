import React from 'react';
import { ToastAndroid, AlertIOS, Platform } from 'react-native';

export const ShowNativeMessage = (title, message, duration) => {
  if (Platform.OS === 'ios') {
    AlertIOS.alert(
      title ? title : 'Alerta',
      message,
    );
  }

  if (Platform.OS === 'android') {
    ToastAndroid.show(message, duration ? (duration === 'SHORT' ? ToastAndroid.SHORT : ToastAndroid.LONG) : ToastAndroid.SHORT);
  }
};