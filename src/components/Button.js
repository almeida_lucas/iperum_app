/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
} from 'react-native';

export default Button = ({
                           title,
                           subTitle,
                           backgroundColor,
                           titleColor,
                           titleSize,
                           subTitleColor,
                           alignText,
                           borderWidth,
                           borderColor,
                           btImg,
                           opacity,
                           btImgStyle,
                         }) => {

  const requireImg = (btImg) => {
    switch (btImg) {
      case 'logoPrincipal':
        return require('../resources/logoPrincipal.png');
      case 'deposito':
        return require('../resources/deposito2.png');
      case 'saque':
        return require('../resources/imgSaque.png');
      default:
        return null;
    }
  };

  const imgSource = requireImg(btImg);

  return (
    <View style={[
      styles.containerPrincipal,
      {
        backgroundColor: backgroundColor ? backgroundColor : '#FFFFFF',
        borderWidth: borderWidth ? borderWidth : 0,
        borderColor: borderColor ? borderColor : 'transparent',
        height: subTitle ? 70 : 50,
        opacity: opacity ? opacity : 1,
      },
    ]}>
      <View style={[
        styles.container,
        {
          flex: 1,
          alignItems: alignText ? alignText : 'center',
        },
      ]}>
        <Text style={[
          styles.title,
          {
            color: titleColor ? titleColor : '#FFFFFF',
            fontSize: titleSize ? titleSize : 18,
          },
        ]}>
          {title}
        </Text>
        {subTitle ?
          <Text style={[styles.subTitle, {color: subTitleColor ? subTitleColor : '#FFFFFF'}]}>
            {subTitle}
          </Text>
          :
          null
        }
      </View>
      {imgSource ?
        <Image source={imgSource} style={[styles.img, btImgStyle]}/>
        :
        null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  containerPrincipal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 5,
    padding: 15,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  title: {
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 12,
  },
  img: {
    width: 130,
    height: 130,
    top: -30,
    right: -15,
    opacity: 0.1,
    position: 'absolute',
  },
});