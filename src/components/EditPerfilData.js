import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import AnimatedTextInput from './animated/AnimatedTextInput';

export default EditPerfilData = ({label, value, onChangeText, type, secureTextEntry}) => {
  return (
    <View style={styles.container}>
      <AnimatedTextInput
        label={label}
        value={value}
        labelFontColor='#AAAAAA'
        borderColor='transparent'
        onChangeText={onChangeText}
        type={type}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    paddingBottom: 0,
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  titulo: {
    fontSize: 14,
    color: '#AAAAAA',
  },
  subTitulo: {
    fontSize: 16,
    color: '#000000',
  },
});