/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
} from 'react-native';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

const width = Dimensions.get('screen').width;
type Props = {
};
export default class Footer extends Component<Props> {

  render() {

    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <TouchOpctyPrevSelect onPress={() => console.warn('ONDE ACEITA?')}>
            <Text style={styles.text}>ONDE ACEITA?</Text>
          </TouchOpctyPrevSelect>
        </View>
        <View style={styles.divider}/>
        <View style={styles.textContainer}>
          <TouchOpctyPrevSelect onPress={() => console.warn('AJUDA')}>
            <Text style={styles.text}>AJUDA</Text>
          </TouchOpctyPrevSelect>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width,
    height: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    width: 120,
    alignItems: 'center',
  },
  text: {
    fontSize: 14,
    color: '#FFF',
  },
  divider: {
    width: 1,
    height: 30,
    borderWidth: 2,
    borderColor: '#FFFFFF',
    marginLeft: 10,
    marginRight: 10,
  },
});