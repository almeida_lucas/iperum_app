/**
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';

export default FooterTerms = () => {

  return (
    <Text style={styles.footerText}>Ao utilizar os serviços da Iperum, você concorda com nossos <Text
      onPress={() => console.warn('Link 1')} style={styles.linkText}>Termos de
      serviços</Text> & nossa <Text
      onPress={() => console.warn('Link 2')} style={styles.linkText}>Política de
      privacidade</Text>
    </Text>
  );
};

const styles = StyleSheet.create({
  footerText: {
    fontSize: 12,
  },
  linkText: {
    color: '#7c176c',
  },
});