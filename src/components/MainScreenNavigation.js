import React from 'react';
import { Navigation } from 'react-native-navigation';

export const MainScreenNavigation = (newUser) => Navigation.startTabBasedApp({
  tabs: [
    {
      label: 'Pagamento', // tab label as appears under the icon in iOS (optional)
      screen: 'Pagamento', // unique ID registered with Navigation.registerScreen
      icon: require('../resources/pagamento-roxo.png'), // local image asset for the tab icon unselected state (optional on iOS)
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
    },
    {
      label: 'Conta',
      screen: 'Conta',
      icon: require('../resources/conta-roxo.png'),
      navigatorStyle: {
        navBarHidden: true,
      },
      //selectedIcon: require('../img/two_selected.png'),
    },
    {
      label: 'Extrato',
      screen: 'Extrato',
      icon: require('../resources/extrato-roxo.png'),
      //selectedIcon: require('../img/two_selected.png'),
    },
  ],
  tabsStyle: {
    tabBarBackgroundColor: '#FFF',
  },
  appStyle: {
    orientation: 'portrait',
    tabBarSelectedButtonColor: '#630851',
    tabFontSize: 10,
  },
  passProps: {
    newUser,
  },
});