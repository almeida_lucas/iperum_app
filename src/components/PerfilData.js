import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

export default PerfilData = ({label, value}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>{label}</Text>
      <Text style={styles.subTitulo}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  titulo: {
    fontSize: 14,
    color: '#AAAAAA',
  },
  subTitulo: {
    fontSize: 16,
    color: '#000000',
  },
});