/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';

import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

type Props = {
  usuario: any,
  email: any,
  saldo: any,
  pontos: any,
};

export default class InitialScreenNavbar extends Component<Props> {

  state = {
    disabled: false,
  };

  mostraPerfil = () => this.props.navigator.showModal({
    screen: 'ModalPerfil',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  disableEvent = () => this.setState({disabled: true}, () => setTimeout(() => this.setState({disabled: false}), 1000));

  render() {
    const {nome = 'Usuário Teste', email = 'usuario.teste@email.com', saldo, pontos, mostraEsconde, mostraEscondeFunc, iniciais = 'UT'} = this.props;

    return (
      <View>
        <View style={styles.container}>
          <TouchOpctyPrevSelect onPressOut={this.disableEvent} onPress={this.mostraPerfil} disabled={this.state.disabled}>
            <View style={styles.usuarioContainer}>
              <View style={styles.iniciaisView}>
                <Text style={styles.iniciais}>{iniciais}</Text>
              </View>
              <View style={styles.tituloContainer}>
                <Text style={styles.titulo}>{nome}</Text>
                <Text style={styles.subTitulo}>{email}</Text>
              </View>
            </View>
          </TouchOpctyPrevSelect>
        </View>
        <View style={styles.saldoContainer}>
          <View>
            <Text style={styles.saldoTexto}>Saldo</Text>
            <Text style={styles.pontosTexto}>Pontos</Text>
          </View>
          {mostraEsconde ?
            <Image style={styles.imgEmEsconde} source={require('../resources/emEsconde.png')}/>
            :
            <View style={styles.saldoContainerValor}>
              <Text style={styles.cifrao}>R$</Text>
              <View style={styles.containerValores}>
                <Text style={styles.saldoValor}>{saldo}</Text>
                <Text style={styles.pontosValor}>{pontos} pontos</Text>
              </View>
            </View>
          }
          <TouchableOpacity onPress={mostraEscondeFunc}>
            {mostraEsconde ?
              <Image style={styles.imgMostraEsconde} source={require('../resources/mostra.png')}/>
              :
              <Image style={styles.imgMostraEsconde} source={require('../resources/esconde.png')}/>
            }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    paddingBottom: 10,
    paddingTop: Platform.OS === 'ios' ? 25 : 10,
    backgroundColor: '#ffc210',
  },
  imgMenu: {
    width: 18,
    height: 23,
  },
  usuarioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iniciaisView: {
    width: 40,
    height: 40,
    borderRadius: 40,
    backgroundColor: '#683A6A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iniciais: {
    fontSize: 16,
    color: '#DDDDDD',
  },
  tituloContainer: {
    justifyContent: 'flex-start',
    paddingLeft: 10,
  },
  titulo: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#862777',
  },
  subTitulo: {
    fontSize: 12,
    color: '#000000',
  },
  saldoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: '#eff2f3',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  saldoTexto: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  pontosTexto: {
    fontSize: 12,
  },
  saldoContainerValor: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 3,
    marginBottom: 2,
  },
  cifrao: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#BBBBBB',
    marginTop: 3,
    marginRight: 5,
  },
  containerValores: {
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  saldoValor: {
    fontSize: 18,
    color: '#000000',
    fontWeight: 'bold',
  },
  pontosValor: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  imgMostraEsconde: {
    width: 23,
    height: 23,
    margin: 10,
  },
  imgEmEsconde: {
    margin: 8,
    width: 25,
    height: 25,
  },
});
