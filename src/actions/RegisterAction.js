import axios from 'axios/index';
import {
  URL_BASE,
  VERIFICA_CPF_SUCESSO,
  VERIFICA_CPF_ERRO,
  VERIFICA_EMAIL_SUCESSO,
  VERIFICA_EMAIL_ERRO,
  VERIFICA_USUARIO_SUCESSO,
  VERIFICA_USUARIO_ERRO,
  BASE64,
  VERIFICA_CNPJ_ERRO,
  VERIFICA_CNPJ_SUCESSO,
} from '../contants';

const retornaErro = async (type, error) => {
  return {type, error};
};

const isCPFValid = (cpf) => {
  let sumFirstDigit = 0;
  let sumLastDigit = 0;
  const cpfSemPontos = cpf.replace('.', '').replace('.', '').replace('-', '');
  if (cpfSemPontos.length !== 11) return false;

  if (cpfSemPontos === '11111111111' || cpfSemPontos === '22222222222'
    || cpfSemPontos === '33333333333' || cpfSemPontos === '44444444444'
    || cpfSemPontos === '55555555555' || cpfSemPontos === '66666666666'
    || cpfSemPontos === '77777777777' || cpfSemPontos === '88888888888'
    || cpfSemPontos === '99999999999' || cpfSemPontos === '00000000000') return false;

  cpfSemPontos.substr(0, 9).split('').filter((char, index) => {
    sumFirstDigit += parseInt(char) * (10 - index);
    sumLastDigit += parseInt(char) * (11 - index);
  });

  const modFirstDigit = sumFirstDigit % 11;
  const firstDigit = cpfSemPontos.substr(9, 1);
  const lastDigit = cpfSemPontos.substr(10, 1);
  const calculatedFirstDigit = 11 - modFirstDigit;

  if (calculatedFirstDigit >= 10) {
    if (firstDigit !== '0') return false;
  } else if (firstDigit !== calculatedFirstDigit.toString()) return false;

  sumLastDigit += firstDigit * 2;
  const modLastDigit = sumLastDigit % 11;
  const calculatedLastDigit = 11 - modLastDigit;

  if (calculatedLastDigit >= 10) return (lastDigit === '0');
  return lastDigit === calculatedLastDigit.toString();
};

const isCNPJValid = (cnpj) => {
  let sumFirstDigit = 0;
  let sumLastDigit = 0;
  if (cnpj.length !== 14) return false;

  const arrCnpj = cnpj.substr(0, 13).split('');
  sumFirstDigit += arrCnpj[0] * 5;
  sumFirstDigit += arrCnpj[1] * 4;
  sumFirstDigit += arrCnpj[2] * 3;
  sumFirstDigit += arrCnpj[3] * 2;
  sumFirstDigit += arrCnpj[4] * 9;
  sumFirstDigit += arrCnpj[5] * 8;
  sumFirstDigit += arrCnpj[6] * 7;
  sumFirstDigit += arrCnpj[7] * 6;
  sumFirstDigit += arrCnpj[8] * 5;
  sumFirstDigit += arrCnpj[9] * 4;
  sumFirstDigit += arrCnpj[10] * 3;
  sumFirstDigit += arrCnpj[11] * 2;

  const modFirstDigit = sumFirstDigit % 11;
  const firstDigit = cnpj.substr(12, 1);
  const lastDigit = cnpj.substr(13, 1);
  const calculatedFirstDigit = 11 - modFirstDigit;

  if (calculatedFirstDigit >= 10) {
    if (firstDigit !== '0') return false;
  } else if (firstDigit !== calculatedFirstDigit.toString()) return false;

  sumLastDigit += arrCnpj[0] * 6;
  sumLastDigit += arrCnpj[1] * 5;
  sumLastDigit += arrCnpj[2] * 4;
  sumLastDigit += arrCnpj[3] * 3;
  sumLastDigit += arrCnpj[4] * 2;
  sumLastDigit += arrCnpj[5] * 9;
  sumLastDigit += arrCnpj[6] * 8;
  sumLastDigit += arrCnpj[7] * 7;
  sumLastDigit += arrCnpj[8] * 6;
  sumLastDigit += arrCnpj[9] * 5;
  sumLastDigit += arrCnpj[10] * 4;
  sumLastDigit += arrCnpj[11] * 3;
  sumLastDigit += arrCnpj[12] * 2;
  const modLastDigit = sumLastDigit % 11;
  const calculatedLastDigit = 11 - modLastDigit;

  if (calculatedLastDigit >= 10) return (lastDigit === '0');
  return lastDigit === calculatedLastDigit.toString();
};

export const verificaCPF = (cpf) => {
  if (cpf === '') return retornaErro(VERIFICA_CPF_ERRO, 'CPF em branco!').then(resposta => resposta);
  if (!isCPFValid(cpf)) return retornaErro(VERIFICA_CPF_ERRO, 'CPF inválido!').then(resposta => resposta);
  return axios.get(`${URL_BASE}/v1/clientes/documento/${cpf}`)
    .then(({data}) => {
      if (data.status) return {type: VERIFICA_CPF_ERRO, error: 'CPF ja existe!'};
      return {type: VERIFICA_CPF_SUCESSO, data};
    })
    .catch(error => {
      console.warn(cpf, error);
      return {type: VERIFICA_CPF_ERRO, error: error.toString()};
    });
};

export const verificaCNPJ = (cnpj) => {
  if (cnpj === '') return retornaErro(VERIFICA_CNPJ_ERRO, 'CNPJ em branco!').then(resposta => resposta);
  const cnpjSemPontos = cnpj.replace('.', '').replace('.', '').replace('/', '').replace('-', '');
  if (!isCNPJValid(cnpjSemPontos)) return retornaErro(VERIFICA_CNPJ_ERRO, 'CNPJ inválido!').then(resposta => resposta);
  return axios.get(`${URL_BASE}/v1/clientes/documento/${cnpjSemPontos}`)
    .then(({data}) => {
      if (data.status) return {type: VERIFICA_CNPJ_ERRO, error: 'CNPJ ja existe!'};
      return {type: VERIFICA_CNPJ_SUCESSO, data};
    })
    .catch(error => {
      return {type: VERIFICA_CNPJ_ERRO, error};
    });
};

export const verificaEmail = (email) => {
  if (email === '') return retornaErro(VERIFICA_EMAIL_ERRO, 'Email em branco!').then(resposta => resposta);

  return axios.get(`${URL_BASE}/v1/clientes/email/${BASE64.encode(email)}`)
    .then(({data}) => {
      if (data.error) return {type: VERIFICA_EMAIL_ERRO, error: data.error};
      if (data.status) return {type: VERIFICA_EMAIL_ERRO, error: 'Email ja existe!'};
      return {type: VERIFICA_EMAIL_SUCESSO, data};
    })
    .catch(error => {
      return {type: VERIFICA_EMAIL_ERRO, error};
    });
};

export const verificaUsuario = (usuario) => {
  if (usuario === '') return retornaErro(VERIFICA_USUARIO_ERRO, 'Usuario em branco!').then(resposta => resposta);

  return axios.get(`${URL_BASE}/v1/clientes/usuario/${usuario}`)
    .then(({data}) => {
      if (data.error) return {type: VERIFICA_USUARIO_ERRO, error: data.error};
      if (data.status) return {type: VERIFICA_USUARIO_ERRO, error: 'Usuario ja existe!'};
      return {type: VERIFICA_USUARIO_SUCESSO, data};
    })
    .catch(error => {
      return {type: VERIFICA_USUARIO_ERRO, error: error.toString()};
    });
};
