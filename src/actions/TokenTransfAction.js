import {
  URL_BASE,
  TOKEN_TRANSF_SUCESSO,
  TOKEN_TRANSF_ERRO, TRANSF_SUCESSO, TRANSF_ERRO,
} from '../contants';
import axios from 'axios/index';

export const buscatokenTransferencia = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/transferencias/clientes/${id}/token`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: TOKEN_TRANSF_SUCESSO,
        token: data.token,
      };
    })
    .catch(({response}) => {
      return {
        type: TOKEN_TRANSF_ERRO,
        erro: response.data.error,
      };
    });
};

export const realizaTransferencia = (id, token, data) => {
  return axios(`${URL_BASE}/api/v1/transferencias/clientes/${id}`, {
    method: 'post',
    data,
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: TRANSF_SUCESSO,
        data,
      };
    })
    .catch(({response}) => {
      console.warn(response);
      return {
        type: TRANSF_ERRO,
        erro: response.data.toString(),
      };
    });
};