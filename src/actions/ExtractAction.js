import { LISTA_EXTRATO_SUCESSO, LISTA_EXTRATO_ERRO, URL_BASE } from '../contants';
import axios from 'axios/index';

export const buscaExtrato = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/clientes/${id}/extrato`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: LISTA_EXTRATO_SUCESSO,
        listaExtrato: data,
      };
    })
    .catch(({response}) => {
      return {
        type: LISTA_EXTRATO_ERRO,
        erro: response.data.error,
      };
    });
};