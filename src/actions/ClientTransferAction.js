import { LISTA_CLIENTES_SUCESSO, LISTA_CLIENTES_ERRO, URL_BASE } from '../contants';
import axios from 'axios/index';

export const buscaClientesTransferencia = (token) => {
  return axios.get(`${URL_BASE}/api/v1/transferencias/clientes`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: LISTA_CLIENTES_SUCESSO,
        listaClientes: data,
      };
    })
    .catch(({response}) => {
      return {
        type: LISTA_CLIENTES_ERRO,
        erro: response.data.error,
      };
    });
};

export const filtraClientes = (listaClientes, filtro) => {
  return {
    type: LISTA_CLIENTES_SUCESSO,
    listaClientes: listaClientes.filter(
      (cliente) =>
        ( cliente.nome && cliente.nome.toLowerCase().includes(filtro.toLowerCase()) )
        || ( cliente.email && cliente.email.toLowerCase().includes(filtro.toLowerCase()) )
        || ( cliente.usuario && cliente.usuario.toLowerCase().includes(filtro.toLowerCase()) )
    ),
  };
};