import { START_INTERVAL } from '../contants';

export const startInterval = (interval) => {
  return {
    type: START_INTERVAL,
    interval
  }
};