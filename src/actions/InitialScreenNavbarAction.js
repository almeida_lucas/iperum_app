import {
  MOSTRA_ESCONDE_SALDO,
} from '../contants';

export const mostraEscondeSaldo = (mostraEsconde) => {
  return {
    type: MOSTRA_ESCONDE_SALDO,
    mostraEsconde: !mostraEsconde,
  };
};