import { LISTA_CARTEIRAS_SUCESSO, LISTA_CARTEIRAS_ERRO, URL_BASE } from '../contants';
import axios from 'axios/index';

export const buscaCarteirasCliente = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/clientes/${id}/carteiras`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: LISTA_CARTEIRAS_SUCESSO,
        carteiras: data,
      };
    })
    .catch(({response}) => {
      return {
        type: LISTA_CARTEIRAS_ERRO,
        erro: response.data.error,
      };
    });
};