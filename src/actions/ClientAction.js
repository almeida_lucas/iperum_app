import axios from 'axios/index';
import {
  URL_BASE,
  BUSCA_CLIENTE_SUCESSO,
  BUSCA_CLIENTE_ERRO,
  LOGIN_USUARIO,
  LOGIN_USUARIO_ERROR,
  SALVA_CLIENTE_ERRO,
  SALVA_CLIENTE_SUCESSO,
  BASE64,
  RESET_SENHA_SUCESSO,
  RESET_SENHA_ERRO, ATUALIZA_CLIENTE_SUCESSO, ATUALIZA_CLIENTE_ERRO,
} from '../contants';
import { AsyncStorage } from 'react-native';

export const loginUsuario = (cliente) => {
  return axios.post(`${URL_BASE}/v1/auth/login`, cliente)
    .then(({data}) => {
      if (data) {
        AsyncStorage.setItem('cliente', JSON.stringify(data));
        return {
          type: LOGIN_USUARIO,
          cliente: data,
        };
      }
    })
    .catch(({response}) => {
      if (response && response.data && response.data.error) return {type: LOGIN_USUARIO_ERROR, error: response.data.error};
      else return {
        type: LOGIN_USUARIO_ERROR,
        error: response.toString(),
      };
    });
};

export const verificaLogin = () => {
  return AsyncStorage.getItem('cliente')
    .then(clienteSerializado => JSON.parse(clienteSerializado))
    .catch(error => {
      console.warn('errorVerificaLogin -> ', error);
      return {
        type: LOGIN_USUARIO_ERROR,
        error,
      };
    })
    .then(cliente => {
      if (cliente) {
        return {
          type: LOGIN_USUARIO,
          cliente,
        };
      }
      return {
        type: LOGIN_USUARIO_ERROR,
        cliente,
      };
    });
};

export const buscaCliente = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/clientes/${id}`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data, error}) => {
      return {
        type: BUSCA_CLIENTE_SUCESSO,
        cliente: data,
      };
    })
    .catch(({response}) => {
      AsyncStorage.clear();
      return {
        type: BUSCA_CLIENTE_ERRO,
        error: response.data.error,
      };
    });
};

export const salvaUsuario = (cliente) => {
  return axios(`${URL_BASE}/v1/clientes`, {
    method: 'post',
    data: cliente,
  })
    .then(({data}) => {
      AsyncStorage.setItem('cliente', JSON.stringify(data));
      return {type: SALVA_CLIENTE_SUCESSO, cliente: data}
    })
    .catch(error => {
      console.warn('error', error);
      return {type: SALVA_CLIENTE_ERRO, error};
    });
};

export const resetSenha = (email) => {
  return axios(`${URL_BASE}/v1/auth/reset/${BASE64.encode(email)}`, {
    method: 'get',
  })
    .then(({data}) => {
      const {status, error} = data;
      if (status) return {type: RESET_SENHA_SUCESSO, status};
      if (error) return {type: RESET_SENHA_ERRO, error};
      return {type: RESET_SENHA_ERRO, error: 'Erro indefinido'};
    })
    .catch(error => {
      return {type: RESET_SENHA_ERRO, error: error.toString()}
    });
};

export const atualizaCliente = (id, token, cliente) => {
  return axios(`${URL_BASE}/api/v1/clientes/${id}`, {
    headers: {
      'Authorization': token,
    },
    method: 'put',
    data: cliente,
  })
    .then(({data}) => {
      return {type: ATUALIZA_CLIENTE_SUCESSO, cliente: data}
    })
    .catch(error => {
      return {type: ATUALIZA_CLIENTE_ERRO, error: error.toString()}
    });
};