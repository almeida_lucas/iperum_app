import axios from 'axios/index';
import {
  URL_BASE,
  BUSCA_SOLICITACOES_SUCESSO,
  BUSCA_SOLICITACOES_ERRO,
  BUSCA_CONTAS_SUCESSO,
  BUSCA_CONTAS_ERRO,
  CADASTRA_CONTA_SUCESSO,
  CADASTRA_CONTA_ERRO,
  NOVA_SOLICITACAO_SUCESSO,
  NOVA_SOLICITACAO_ERRO,
} from '../contants';

export const buscaSolicitacoes = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/solicitacoes/clientes/${id}`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: BUSCA_SOLICITACOES_SUCESSO,
        solicitacoes: data,
      };
    })
    .catch(({response}) => {
      return {
        type: BUSCA_SOLICITACOES_ERRO,
        error: response.error,
      };
    });
};

export const novaSolicitacao = (data, id, token) => {
  return axios(`${URL_BASE}/api/v1/solicitacoes/clientes/${id}`, {
    method: 'post',
    data,
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      if (data.status) {
        return {
          type: NOVA_SOLICITACAO_SUCESSO,
          sucesso: data,
        };
      }
      return {
        type: NOVA_SOLICITACAO_ERRO,
        error: data.error,
      };
    })
    .catch(({response}) => {
      return {
        type: NOVA_SOLICITACAO_ERRO,
        error: response.error,
      };
    });
};

export const buscaContasCadastradas = (id, token) => {
  return axios.get(`${URL_BASE}/api/v1/clientes/${id}/contas`, {
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: BUSCA_CONTAS_SUCESSO,
        contas: data,
      };
    })
    .catch(({response}) => {
      return {
        type: BUSCA_CONTAS_ERRO,
        error: response.error,
      };
    });
};

export const cadastraConta = (data, id, token, callbackSucesso) => {
  return axios(`${URL_BASE}/api/v1/clientes/${id}/contas`, {
    method: 'post',
    data,
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      callbackSucesso();
      return {
        type: CADASTRA_CONTA_SUCESSO,
        contas: data,
      };
    })
    .catch(({response}) => {
      return {
        type: CADASTRA_CONTA_ERRO,
        error: response.data.error,
      };
    });
};

export const deletaConta = (id, token, conta, digito_conta) => {
  return axios(`${URL_BASE}/api/v1/clientes/${id}/contas/${conta}/${digito_conta}`, {
    method: 'delete',
    headers: {
      'Authorization': token,
    },
  })
    .then(({data}) => {
      return {
        type: 'DELETE_CONTAS_SUCESSO',
        status: data.status,
      };
    })
    .catch((response) => {
      console.warn('TODO - criar alerta notificação', response);
      return {
        type: 'DELETE_CONTA_ERRO',
        error: response,
      };
    });
};