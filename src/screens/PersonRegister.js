/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';
import InputError from '../components/InputError';
import { connect } from 'react-redux';
import { verificaCPF, verificaEmail } from '../actions/RegisterAction';
import {
  LONG,
  VERIFICA_CPF_ERRO,
  VERIFICA_CPF_SUCESSO,
  VERIFICA_EMAIL_ERRO,
  VERIFICA_EMAIL_SUCESSO,
} from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';
import {Navigation} from 'react-native-navigation';

class PersonRegister extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
  };

  constructor() {
    super();

    this.state = {
      nome: '',
      cpf: '',
      email: '',
      celular: '',
      msgErro: '',
      msgErrCpf: '',
      msgErrEmail: '',
    };
  }

  componentDidMount() {
    this.setState({
      nome: '',
      cpf: '',
      email: '',
      celular: '',
      msgErro: '',
      msgErrCpf: '',
      msgErrEmail: '',
    });
  }

  onChangeCPF = (cpf) => this.setState({cpf});
  onChangeEmail = (email) => this.setState({email});
  onChangeTelefone = (celular) => this.setState({celular});

  verificaCPF = () => this.props.verificaCPF(this.state.cpf)
    .then(({type, error, data}) => {
      if (type === VERIFICA_CPF_ERRO) this.setState({msgErrCpf: error});
      else if (type === VERIFICA_CPF_SUCESSO) this.setState({msgErrCpf: ''});
      return {type, error, data};
    });

  verificaEmail = () => this.props.verificaEmail(this.state.email)
    .then(({type, error, data}) => {
      if (type === VERIFICA_EMAIL_ERRO) this.setState({msgErrEmail: error});
      else if (type === VERIFICA_EMAIL_SUCESSO) this.setState({msgErrEmail: ''});
      return {type, error, data};
    });

  _continuaLogin = () => Navigation.setRoot({
    root: {
      component: {
        name: "CadastroUsuario",
        title: 'CadastroUsuario',
      },
    },
  });

  continuar = () => {
    const {nome, cpf, email, celular} = this.state;

    if (nome === '' || cpf === '' || email === '' || celular === '') {
      this.setState({msgErro: '*Todos os campos devem ser preenchidos!'});
    } else {
      this.verificaCPF()
        .then(({type}) => {
          if (type === VERIFICA_CPF_ERRO) ShowNativeMessage('Cadastro', 'Insira um CPF válido para seguir', LONG);
          else if (type === VERIFICA_CPF_SUCESSO) this.verificaEmail()
            .then(({type}) => {
              if (type === VERIFICA_EMAIL_ERRO) ShowNativeMessage('Cadastro', 'Insira um EMAIL válido para seguir', LONG);
              else if (type === VERIFICA_EMAIL_SUCESSO) {
                const cliente = {
                  nome,
                  documento: cpf,
                  email,
                  tipo_cliente: 'F',
                  telefone: celular,
                };

                this.props.navigator.push({
                  screen: 'CadastroUsuario',
                  passProps: {
                    cliente,
                  },
                  backButtonTitle: '',
                });
              }
            });
        });
    }
  };

  render() {
    const {nome, cpf, email, celular, msgErro, msgErrCpf, msgErrEmail} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.btnContainer}>
          <Text style={styles.title}>Fale um pouco sobre você...</Text>

          <TextInput
            style={styles.textInput}
            placeholder="Nome completo"
            value={nome}
            onChangeText={(nome) => this.setState({nome})}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            underlineColorAndroid="transparent"
          />

          <InputError
            onChangeText={this.onChangeCPF}
            msgErr={msgErrCpf}
            type='cpf'
            placeholder='CPF'
            value={cpf}
            onBlur={this.verificaCPF}
            returnKeyLabel='Próximo'
            maxLength={14}
          />

          <InputError
            onChangeText={this.onChangeEmail}
            msgErr={msgErrEmail}
            placeholder='Email'
            value={email}
            onBlur={this.verificaEmail}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            maxLength={255}
          />

          <InputError
            onChangeText={this.onChangeTelefone}
            type='cel-phone'
            placeholder='Celular (com DDD)'
            value={celular}
            returnKeyLabel='Próximo'
            maxLength={15}
          />

          <Text style={styles.msgErro}>{msgErro}</Text>

          <TouchOpctyPrevSelect
            onPress={this._continuaLogin}>
            <Button
              title='CONTINUAR'
              backgroundColor='#ffc210'
              titleSize={16}
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    verificaEmail: (email) => dispatch(verificaEmail(email)),
    verificaCPF: (cpf) => dispatch(verificaCPF(cpf)),
  };
};

export default connect(null, mapDispatchToProps)(PersonRegister);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 10,
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: '#ffc210',
    color: '#000000',
    paddingBottom: 10,
  },
  title: {
    fontSize: 26,
    color: '#000000',
    marginBottom: 20,
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
