/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';
import { verificaCNPJ } from '../actions/RegisterAction';
import { connect } from 'react-redux';
import InputError from '../components/InputError';
import {
  SHORT,
  VERIFICA_CNPJ_ERRO,
  VERIFICA_CNPJ_SUCESSO,
} from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';

class CompanyRegister extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
  };

  state = {
    nome: '',
    nome_fantasia: '',
    documento: '',
    msgErro: '',
    msgErrCNPJ: '',
  };

  componentDidMount() {
    this.setState({
      nome: '',
      nome_fantasia: '',
      documento: '',
      msgErro: '',
      msgErrCNPJ: '',
    });
  }

  onChangeCNPJ = (documento) => this.setState({documento});
  onChangeNomeFantasia = (nome_fantasia) => this.setState({nome_fantasia});
  onChangeNome = (nome) => this.setState({nome});

  verificaCNPJ = () => this.props.verificaCNPJ(this.state.documento)
    .then(({type, data, error}) => {
      if (type === VERIFICA_CNPJ_ERRO) this.setState({msgErrCNPJ: error});
      else if (type === VERIFICA_CNPJ_SUCESSO) this.setState({msgErrCNPJ: ''});
      return {type, data, error};
    });

  continuar = () => {
    const {nome, documento, nome_fantasia} = this.state;

    if (nome === '' || documento === '' || nome_fantasia === '') {
      this.setState({msgErro: '*Todos os campos devem ser preenchidos!'});
    } else {
      this.verificaCNPJ()
        .then(({type, data, error}) => {
          if (type === VERIFICA_CNPJ_ERRO) ShowNativeMessage('Erro', 'Insira um CNPJ válido para seguir', SHORT);
          else if (type === VERIFICA_CNPJ_SUCESSO) {
            const cliente = {
              nome,
              documento,
              nome_fantasia,
            };

            this.props.navigator.push({
              screen: 'CadastroEmpresaPessoa',
              passProps: {
                cliente,
              },
              backButtonTitle: '',
            });
          }
        });
    }
  };

  render() {
    const {nome, nome_fantasia, documento, msgErro, msgErrCNPJ} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.btnContainer}>
          <Text style={styles.title}>Fale um pouco sobre a sua empresa</Text>

          <InputError
            onChangeText={this.onChangeCNPJ}
            msgErr={msgErrCNPJ}
            type='cnpj'
            placeholder='CNPJ'
            value={documento}
            onBlur={this.verificaCNPJ}
            returnKeyLabel='Próximo'
            maxLength={18}
            returnKeyType='done'
          />

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholder="Nome da empresa"
            value={nome}
            onChangeText={this.onChangeNome}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            returnKeyType='done'
          />

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholder="Nome fantasia"
            value={nome_fantasia}
            onChangeText={this.onChangeNomeFantasia}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            returnKeyType='done'
          />

          <Text style={styles.msgErro}>{msgErro}</Text>

          <TouchOpctyPrevSelect
            onPress={this.continuar}>
            <Button
              title='CONTINUAR'
              backgroundColor='#ffc210'
              titleSize={20}
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    verificaCNPJ: (cpf) => dispatch(verificaCNPJ(cpf)),
  };
};

export default connect(null, mapDispatchToProps)(CompanyRegister);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 15,
    fontSize: 14,
    borderColor: '#FFFFFF',
    color: '#000000',
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#ffc210',
  },
  title: {
    fontSize: 26,
    color: '#000000',
    marginBottom: 30,
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
