/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
} from 'react-native';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';
import {Navigation} from 'react-native-navigation';

export default class Welcome extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
  };

  componentDidMount() {
  }

  contaPessoal = () => Navigation.setRoot({
    root: {
      component: {
        name: "CadastroPessoa",
        title: 'CadastroPessoa',
      },
    },
  });

  contaEmpresa = () => Navigation.setRoot({
    root: {
      component: {
        name: "CadastroEmpresa",
        title: 'CadastroEmpresa',
      },
    },
  });

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Olá! </Text>
            <Image style={styles.imgTitle}
                   source={require('../resources/hand.png')}/>
          </View>
          <Text style={styles.title}>Bem vind@ à Iperum!</Text>
          <Text style={styles.subTitle}>Deseja criar uma conta pessoal ou uma conta empresa?</Text>

          <View style={styles.btnContainer}>
            <TouchOpctyPrevSelect
              onPress={this.contaPessoal}>
              <Button
                title='Conta pessoal'
                subTitle='(pessoa física)'
                backgroundColor='#ff9c01'
                alignText='flex-start'
                btImg='logoPrincipal'
              />
            </TouchOpctyPrevSelect>
            <TouchOpctyPrevSelect
              onPress={this.contaEmpresa}>
              <Button
                title='Conta empresa'
                subTitle='(pessoa jurídica)'
                backgroundColor='#630851'
                alignText='flex-start'
                btImg='logoPrincipal'
              />
            </TouchOpctyPrevSelect>
          </View>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    backgroundColor: '#FFFFFF',
    padding: 20,
  },
  topContainer: {
    marginTop: 40,
    alignItems: 'stretch',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 26,
    color: '#000000',
  },
  subTitle: {
    fontSize: 18,
    marginTop: 15,
    marginBottom: 15,
  },
  imgTitle: {
    width: 30,
    height: 30,
  },
  footerContainer: {
    flexDirection: 'row',
  },
  btnContainer: {
    height: 180,
    justifyContent: 'space-around',
    alignItems: 'stretch',
  },
});
