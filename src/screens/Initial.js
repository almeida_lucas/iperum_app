/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
  Platform,
  Dimensions,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import Footer from '../components/Footer';
import {Navigation} from 'react-native-navigation';

const width = Dimensions.get('screen').width;
const btnCreateAccTitle = Platform.OS === 'android' ? 'Criar conta'.toUpperCase() : 'Criar conta';
const btnAccCreatedTitle = Platform.OS === 'android' ? 'Já tenho conta'.toUpperCase() : 'Ja tenho conta';

export default class Initial extends Component<{}> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  componentDidMount() {
  }

  createAccount = () => {
    Navigation.setRoot({
      root: {
        component: {
          name: "Welcome",
          title: 'Welcome',
        },
      },
    });
  };

  login = () => Navigation.setRoot({
    root: {
      component: {
        name: "Login",
        title: 'Login',
      },
    },
  });

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Image style={styles.imgLogo} source={require('../resources/logo.png')}/>

          <View style={styles.btnContainer}>
            <TouchOpctyPrevSelect
              onPress={this.createAccount}>
              <Button
                title={btnCreateAccTitle}
                backgroundColor='#630851'
                titleSize={16}
              />
            </TouchOpctyPrevSelect>
            <TouchOpctyPrevSelect
              onPress={this.login}>
              <Button
                title={btnAccCreatedTitle}
                backgroundColor='transparent'
                titleSize={16}
                borderWidth={2}
                borderColor='#FFFFFF'
              />
            </TouchOpctyPrevSelect>
          </View>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ffc210',
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    marginBottom: 60,
  },
  btnContainer: {
    width,
    height: 120,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    paddingLeft: 30,
    paddingRight: 30,
  },
});
