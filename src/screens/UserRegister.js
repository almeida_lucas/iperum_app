/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { verificaUsuario } from '../actions/RegisterAction';
import { verificaLogin, salvaUsuario } from '../actions/ClientAction';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import { ShowNativeMessage } from '../components/NativeMessage';
import InputError from '../components/InputError';
import FooterTerms from '../components/FooterTerms';
import { LONG, SALVA_CLIENTE_SUCESSO, VERIFICA_USUARIO_ERRO, VERIFICA_USUARIO_SUCESSO } from '../contants';
import { MainScreenNavigation } from '../components/MainScreenNavigation';

class UserRegister extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
  };

  constructor() {
    super();

    this.state = {
      usuario: '',
      senha: '',
      confSenha: '',
      msgErro: '',
      msgErrUsuario: '',
    };
  }

  componentDidMount() {
    this.setState({
      usuario: '',
      senha: '',
      confSenha: '',
      msgErro: '',
      msgErrUsuario: '',
    });
  }

  onChangeUsuario = (usuario) => this.setState({usuario});
  verificaUsuario = () => this.props.verificaUsuario(this.state.usuario)
    .then(({type, data, error}) => {
      if (type === VERIFICA_USUARIO_ERRO) this.setState({msgErrUsuario: error});
      else if (type === VERIFICA_USUARIO_SUCESSO) this.setState({msgErrUsuario: ''});
      return {type, data, error};
    });

  cadastroErro = () => {
    console.warn('TODO - cadastroErro');
  };

  _continuaLogin = () => Navigation.setRoot({
    root: {
      stack: {
        id: `StackScreens`,
        children: [
          {
            sideMenu: {
              center: {
                bottomTabs: {
                  id: 'BottomTabs',
                  name: 'BottomTabs',
                  options: {
                    bottomTabs: {
                      backgroundColor: '#FFF',
                      // currentTabIndex: 0,
                    },
                  },
                  children: [
                    {
                      component: {
                        id: 'Pagamento',
                        name: 'Pagamento',
                        options: {
                          bottomTab: {
                            text: 'Pagamento',
                            icon: require('../resources/pagamento-roxo.png'),
                            iconColor: '#888',
                            selectedIconColor: '#B54D80',
                            textColor: '#888',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                    {
                      component: {
                        id: 'Conta',
                        name: 'Conta',
                        options: {
                          bottomTab: {
                            text: 'Conta',
                            icon: require('../resources/conta-roxo.png'),
                            iconColor: '#888',
                            selectedIconColor: '#B54D80',
                            textColor: '#888',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                    {
                      component: {
                        id: 'Extrato',
                        name: 'Extrato',
                        options: {
                          bottomTab: {
                            text: 'Extrato',
                            icon: require('../resources/extrato-roxo.png'),
                            iconColor: '#888',
                            selectedIconColor: '#B54D80',
                            textColor: '#888',
                            selectedTextColor: '#B54D80',
                            fontFamily: 'Helvetica',
                            fontSize: 10,
                          },
                        },
                      },
                    },
                  ],
                },
              }
            }
          },
        ],
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false,
          },
        },
      }
    },
  });

  continuar = () => {
    const {usuario, senha, confSenha} = this.state;

    if (usuario === '' || senha === '' || confSenha === '') {
      this.setState({
        msgErro: '*Todos os campos devem ser preenchidos!',
      });
    } else if (senha !== confSenha) {
      this.setState({
        msgErro: '*Senha e confirmaçao de senha devem ser iguais!',
      });
    } else {
      this.verificaUsuario()
        .then(({type, data, error}) => {
          if (type === VERIFICA_USUARIO_ERRO) ShowNativeMessage('Cadastro', 'Insira um USUARIO válido para seguir', LONG);
          else if (type === VERIFICA_USUARIO_SUCESSO) {
            this.props.salvaUsuario({
              ...this.props.cliente,
              usuario,
              senha,
            }).then(({type, cliente}) => {
              if (type === SALVA_CLIENTE_SUCESSO) MainScreenNavigation(true);
              else this.cadastroErro();
            });
          }
        });
    }
  };

  render() {
    const {usuario, senha, confSenha, msgErro, msgErrUsuario} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.btnContainer}>
          <Text style={styles.title}>Escolha um nome de usuário e uma senha para sua conta</Text>

          <InputError
            onChangeText={this.onChangeUsuario}
            msgErr={msgErrUsuario}
            placeholder='Nome usuário'
            value={usuario}
            onBlur={this.verificaUsuario}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
          />

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholder="Crie uma senha"
            value={senha}
            onChangeText={(senha) => this.setState({senha})}
            secureTextEntry={true}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            returnKeyType='done'
          />

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholder="Confirme a senha"
            value={confSenha}
            onChangeText={(confSenha) => this.setState({confSenha})}
            secureTextEntry={true}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            returnKeyType='done'
          />

          <Text style={styles.msgErro}>{msgErro}</Text>

          <TouchOpctyPrevSelect
            onPress={this._continuaLogin}>
            <Button
              title='CONTINUAR'
              backgroundColor='#ffc210'
              titleSize={16}
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    salvaUsuario: (cliente) => dispatch(salvaUsuario(cliente)),
    verificaUsuario: (usuario) => dispatch(verificaUsuario(usuario)),
  };
};

export default connect(null, mapDispatchToProps)(UserRegister);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 20,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 10,
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: '#ffc210',
    color: '#000000',
    height: 40,
    paddingBottom: 10,
  },
  title: {
    fontSize: 26,
    color: '#000000',
    marginBottom: 20,
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
