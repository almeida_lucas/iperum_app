/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { verificaLogin } from '../actions/ClientAction';
import { MainScreenNavigation } from '../components/MainScreenNavigation';
import { LOGIN_USUARIO, LOGIN_USUARIO_ERROR, LONG } from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';
import {Navigation} from 'react-native-navigation';

type Props = {};

class SplashScreen extends Component<Props> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  navegaTelaInicial = () => Navigation.setRoot({
    root: {
      component: {
        name: "Initial",
        title: 'Initial',
      },
    },
  });

  componentDidMount() {
    setTimeout(
      this.navegaTelaInicial
      /*() => this.props.verificaLogin()
        .then(response => {
          if (response.type === LOGIN_USUARIO) {
            MainScreenNavigation();
          } else if (response.type === LOGIN_USUARIO_ERROR) {
            this.navegaTelaInicial();
          }
        })
        .catch(error => ShowNativeMessage('Erro', error.toString(), LONG))*/
      , 2000);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../resources/logo.png')}/>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    verificaLogin: () => dispatch(verificaLogin()),
  };
};

export default connect(null, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffc210',
  },
});
