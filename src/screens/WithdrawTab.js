import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';
import { Navigation } from 'react-native-navigation';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';

export default TabSaques = ({contas, cliente}) => {
  const listaExtrato = [{tipo: 'Saque', valor: '23,00', mensagem: 'Saque realizado!', data: '23/11/9999'}];
  const mostraModalSaque = () => Navigation.showModal({
    screen: 'WithdrawModal',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const mostraModalCadastraContaSaque = () => Navigation.showModal({
    screen: 'ModalCadastraContaSaque',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const listaSolicitacoes = listaExtrato.filter(solicitacao => solicitacao.tipo === 'Saque');
  const existeContas = contas.length >= 0;

  const keyExtractor = (item, index) => index.toString();

  const verContasCadastradas = () => Navigation.showModal({
    screen: 'ModalEnviarPara',
    navigatorStyle: {
      navBarHidden: true,
    },
  });

  const renderItem = ({item}) => {
    return (
      <View style={styles.depositoItem}>
        <View style={styles.conteudoDeposito}>
          <Image style={styles.imgDeposito} source={require('../resources/imgSaque2.png')}/>
          <View style={styles.descricaoContainer}>
            <Text style={styles.valorTexto}>R$ {item.valor}</Text>
            <Text style={styles.data}>{item.mensagem}</Text>
          </View>
        </View>
        <Text style={styles.data}>{item.data}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.btContainer}>
        <TouchOpctyPrevSelect
          onPress={mostraModalSaque}>
          <Button
            title='Sacar'
            subTitle='para conta corrente'
            backgroundColor={'#630851'}
            alignText='flex-start'
            btImg='saque'
            btImgStyle={{height: 60, width: 60, opacity: 1, top: 5, right: 10}}
          />
        </TouchOpctyPrevSelect>
      </View>
      {existeContas ?
        <View style={{flex: 1}}>
          <View style={styles.containerTituloSaque}>
            <Text style={styles.saqueTituloTexto}>Saques</Text>
            <TouchOpctyPrevSelect onPress={verContasCadastradas}>
              <Text style={styles.linkContasCadastradas}>Ver contas cadastradas</Text>
            </TouchOpctyPrevSelect>
          </View>
          {listaSolicitacoes.length > 0 ?
            <FlatList
              style={styles.flatList}
              data={listaSolicitacoes}
              keyExtractor={keyExtractor}
              renderItem={renderItem}
            />
            :
            <Text style={{marginLeft: 15}}>Seus saques serão listados aqui</Text>
          }
        </View>
        :
        <View style={styles.containerCadastroConta}>
          <Text style={styles.saqueTexto}>Para realizar saques para a conta corrente, é necessário que tenha no mínimo
            uma conta cadastrada no seu perfil.</Text>
          <TouchOpctyPrevSelect
            onPress={mostraModalCadastraContaSaque}>
            <Button
              title='CADASTRAR UMA CONTA'
              titleColor='#630851'
              backgroundColor='transparent'
              borderWidth={2}
              borderColor='#630851'
              titleSize={16}
            />
          </TouchOpctyPrevSelect>
        </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  btContainer: {
    alignItems: 'stretch',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  containerCadastroConta: {
    alignItems: 'stretch',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 15,
  },
  saqueTexto: {
    marginBottom: 20,
    fontSize: 14,
    color: '#888888',
  },
  saqueTituloTexto: {
    padding: 15,
    paddingTop: 10,
    paddingBottom: 10,
    color: '#888888',
    fontSize: 15,
  },
  flatList: {
    marginBottom: 20,
  },
  depositoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  conteudoDeposito: {
    flexDirection: 'row',
  },
  imgDeposito: {
    width: 35,
    height: 35,
  },
  descricaoContainer: {
    paddingLeft: 5,
  },
  valorTexto: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
  },
  statusDepositoTexto: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    backgroundColor: 'orange',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 20,
  },
  data: {
    fontSize: 12,
    color: '#888888',
  },
  linkContasCadastradas: {
    padding: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 15,
    color: '#630851',
  },
  containerTituloSaque: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
});
