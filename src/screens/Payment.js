/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal, Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { mostraEscondeSaldo } from '../actions/InitialScreenNavbarAction';
import { buscaClientesTransferencia } from '../actions/ClientTransferAction';
import { buscaCarteirasCliente } from '../actions/WalletAction';
import { buscatokenTransferencia } from '../actions/TokenTransfAction';
import { buscaExtrato } from '../actions/ExtractAction';
import { buscaCliente } from '../actions/ClientAction';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import InitialScreenNavbar from '../navbars/InitialScreenNavbar';
import ActivityIndicatorModal from '../modals/ActivityIndicatorModal';
import { ShowNativeMessage } from '../components/NativeMessage';
import PresentationModal from '../modals/PresentationModal';
import { TextInputMask } from 'react-native-masked-text';
import { buscaSolicitacoes } from '../actions/AccountAction';
import { startInterval } from '../actions/IntervalAction';
import { BUSCA_CLIENTE_ERRO, BUSCA_CLIENTE_SUCESSO, LONG, SHORT, tokenExpirado } from '../contants';
import { Navigation } from 'react-native-navigation';

class Payment extends Component<{}> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  state = {
    valorPagamento: '0,00',
    clienteSelecionado: null,
    pagarParaDesabilitado: false,
    payToPressed: false,
    isPayDisabled: false,
    payBtPressed: false,
    newUser: false,
    showDeposit: false,
  };

  componentDidMount() {
    const {id, token} = this.props.cliente;
    const {newUser} = this.props;

    if (newUser) this.setState({newUser});

    this.props.buscaCarteiras(id, token);

    let interval = setInterval(() => {
      const {id, token} = this.props.cliente;
      this.props.buscaCliente(id, token)
        .then(({type, error}) => {
          if (type === BUSCA_CLIENTE_SUCESSO) {
            this.props.buscaCarteiras(id, token);
            this.props.buscaTransacoes(id, token);
          } else if (type === BUSCA_CLIENTE_ERRO) {
            clearInterval(this.props.interval);
            tokenExpirado('Erro', error, LONG);
          }
        });
    }, 20000);
    this.props.startInterval(interval);
  }

  efetuarPagamento = () => {
    this.setState({payBtPressSelected: true});
    const {clienteSelecionado, valorPagamento} = this.state;
    if (parseInt(valorPagamento) < 1) {
      ShowNativeMessage('Pagamento', 'Necessário valor maior que R$ 1,00 para pagamentos.', SHORT);
      this.setState({isPayDisabled: false});
    } else {
      const {id, token} = this.props.cliente;
      this.props.buscaTokenTransf(id, token)
        .then(() => {
            this.props.navigator.showModal({
              screen: 'ConfirmPaymentModal',
              navigatorStyle: {
                navBarHidden: true,
              },
              passProps: {
                valorPagamento,
                clienteSelecionado,
                carteiras: this.props.carteiras,
                limpaDadosPagamento: this.limpaDadosPagamento,
              },
            });
            setTimeout(() => this.setState({isPayDisabled: false}), 500);
          },
        );
    }
  };

  limpaDadosPagamento = () => this.setState({
    valorPagamento: '0,00',
    clienteSelecionado: null,
  });

  selecionaContaPagamento = (clienteSelecionado) => this.setState({clienteSelecionado});

  callbackSucesso = () => {
    this.setState({pagarParaDesabilitado: false}, () => {
      if (Platform.OS === 'android')
        this.props.navigator.showModal({
          screen: 'ModalPagarPara',
          navigatorStyle: {
            navBarHidden: true,
          },
          passProps: {
            selecionaContaPagamento: this.selecionaContaPagamento,
            listaClientesParam: this.props.listaClientes,
          },
        });
    });
  };

  modalPagarPara = () => {
    this.setState({payToPressed: true});
    const {id, token} = this.props.cliente;
    this.props.buscaCliente(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO)
          this.props.buscaClientesTransferencia(this.props.cliente.token)
            .then(this.callbackSucesso);
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(this.props.interval);
          tokenExpirado('Erro', error, LONG);
        }
      });
  };

  disablePayTo = () => {
    this.setState(
      {pagarParaDesabilitado: true},
      () => setTimeout(() => {
        if (this.state.payToPressed) this.setState({payToPressed: false});
        else this.setState({pagarParaDesabilitado: false});
      }, 500));
  };

  disablePayButton = () => {
    this.setState(
      {isPayDisabled: true},
      () => setTimeout(() => {
        if (this.state.payBtPressed) this.setState({payBtPressed: false});
        else this.setState({isPayDisabled: false});
      }, 500));
  };

  mostraEscondeFunc = () => {
    this.props.mostraEscondeSaldo(this.props.mostraEsconde);
  };

  onChangeText = (valorPagamento) => this.setState({valorPagamento});

  closePresentationModal = () => this.setState({newUser: false});

  closeModalOpenDeposit = () => this.setState({newUser: false, showDeposit: true}, () => {
    this.props.navigator.switchToTab({
      tabIndex: 1,
    });

    if (Platform.OS === 'android')
      Navigation.showModal({
        screen: 'ModalDeposito',
        navigatorStyle: {
          navBarHidden: true,
        },
      });
  });

  closeModalOpenAccountTab = () => this.setState({newUser: false}, () => {
    this.props.navigator.switchToTab({
      tabIndex: 1,
    });
  });

  render() {
    const {nome, email, saldo, pontos, iniciais} = this.props.cliente;
    const {mostraEsconde, carteiras} = this.props;
    const {valorPagamento, clienteSelecionado, pagarParaDesabilitado, isPayDisabled, newUser, showDeposit} = this.state;
    return (
      <View style={styles.container}>
        <View>
          <InitialScreenNavbar
            nome={nome}
            email={email}
            saldo={carteiras.length > 0 ? carteiras[carteiras.length - 1].total : '0,00'}
            pontos={carteiras.length > 0 ? carteiras[carteiras.length - 1].pontos : '0,00'}
            navigator={this.props.navigator}
            iniciais={iniciais}
            mostraEsconde={mostraEsconde}
            mostraEscondeFunc={this.mostraEscondeFunc}
          />
          <TouchOpctyPrevSelect onPressOut={this.disablePayTo} onPress={this.modalPagarPara}
                                disabled={pagarParaDesabilitado}>
            <View style={styles.pagarParaContainer}>
              <Text style={styles.pagarParaTexto}>Pagar para:</Text>
              {clienteSelecionado ?
                <UserItem {...clienteSelecionado}/>
                :
                null
              }
            </View>
          </TouchOpctyPrevSelect>
          <View style={styles.containerValorPagamento}>
            <Text>Valor do Pagamento (R$)</Text>
            <TextInputMask
              type='money'
              value={valorPagamento}
              onChangeText={this.onChangeText}
              options={{unit: ''}}
              underlineColorAndroid="transparent"
              style={{alignSelf: 'stretch', textAlign: 'right', color: '#AAAAAA', fontSize: 40}}
              maxLength={9}
              returnKeyType='done'
            />
          </View>
        </View>
        <View style={styles.footer}>
          <TouchOpctyPrevSelect
            onPressOut={this.disablePayButton}
            onPress={this.efetuarPagamento}
            disabled={!clienteSelecionado || isPayDisabled}>
            <Button
              title='PAGAR'
              backgroundColor='#ffc210'
              titleSize={16}
              opacity={!clienteSelecionado || isPayDisabled ? 0.8 : 1}
            />
          </TouchOpctyPrevSelect>
        </View>
        <Modal
          animationType='fade'
          transparent={true}
          visible={newUser}
          onRequestClose={() => {
          }}
          onDismiss={() => {
            if (showDeposit) Navigation.showModal({
              screen: 'ModalDeposito',
              navigatorStyle: {
                navBarHidden: true,
              },
            });
          }}>
          <PresentationModal
            nome={nome}
            closeModal={this.closePresentationModal}
            closeModalOpenDeposit={this.closeModalOpenDeposit}
            closeModalOpenAccountTab={this.closeModalOpenAccountTab}
          />
        </Modal>
        <ActivityIndicatorModal
          visible={pagarParaDesabilitado}
          onDismiss={() => {
            Navigation.showModal({
              screen: 'ModalPagarPara',
              navigatorStyle: {
                navBarHidden: true,
              },
              passProps: {
                selecionaContaPagamento: this.selecionaContaPagamento,
                listaClientesParam: this.props.listaClientes,
              },
            });
          }}/>
      </View>
    );
  }
}

const mapStateToProps = ({cliente, navBarTelaInicial, listaClientes, carteiras, interval}) => {
  const {mostraEsconde} = navBarTelaInicial;
  return {
    cliente,
    mostraEsconde,
    listaClientes,
    carteiras,
    interval,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    mostraEscondeSaldo: (mostraEsconde) => dispatch(mostraEscondeSaldo(mostraEsconde)),
    buscaClientesTransferencia: (token) => dispatch(buscaClientesTransferencia(token)),
    buscaCliente: (id, token) => dispatch(buscaCliente(id, token)),
    buscaCarteiras: (id, token) => dispatch(buscaCarteirasCliente(id, token)),
    buscaTokenTransf: (id, token) => dispatch(buscatokenTransferencia(id, token)),
    buscaTransacoes: (id, token) => dispatch(buscaExtrato(id, token)),
    buscaSolicitacoes: (id, token, callbackErro) => dispatch(buscaSolicitacoes(id, token, callbackErro)),
    startInterval: (interval) => dispatch(startInterval(interval)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  containerValorPagamento: {
    padding: 15,
    paddingBottom: 0,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  pagarParaContainer: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
    paddingTop: 15,
    paddingBottom: 15,
  },
  pagarParaTexto: {
    paddingLeft: 20,
    fontSize: 16,
    color: '#BBBBBB',
  },
  contaSelecionada: {
    fontSize: 20,
    color: '#000000',
    fontWeight: 'bold',
  },
  footer: {
    padding: 20,
  },
});
