/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  Platform,
  TextInput,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import Footer from '../components/Footer';
import { ShowNativeMessage } from '../components/NativeMessage';
import { connect } from 'react-redux';
import { loginUsuario } from '../actions/ClientAction';
import { LOGIN_USUARIO, LOGIN_USUARIO_ERROR, SHORT } from '../contants';
import { MainScreenNavigation } from '../components/MainScreenNavigation';

const btnLoginTitle = Platform.OS === 'android' ? 'Entrar'.toUpperCase() : 'Entrar';

class Login extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#FFFFFF',
    navBarBackgroundColor: '#ffc210',
  };

  constructor() {
    super();

    this.state = {
      login: '',
      senha: '',
    };
  }

  componentDidMount() {
  }

  onChangeLogin = (login) => this.setState({login});
  onChangeSenha = (senha) => this.setState({senha});

  exibeErro = (msgErro) => {
    ShowNativeMessage('Login', msgErro, SHORT);
  };

  login = () => {
    const {login, senha} = this.state;
    this.props.loginUsuario({login, senha})
      .then(({type, error}) => {
        if (type === LOGIN_USUARIO) MainScreenNavigation();
        else if (type === LOGIN_USUARIO_ERROR) this.exibeErro(error);
      })
      .catch(({type, error}) => {
        if (type === LOGIN_USUARIO_ERROR) this.exibeErro(error);
      });
  };

  esqueciMinhaSenha = () => {
    this.props.navigator.push({
      screen: 'EsqueciMinhaSenha',
      backButtonTitle: '',
    });
  };

  render() {
    const {login, senha} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require('../resources/logo.png')}/>
        </View>
        <View style={styles.btnContainer}>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholderTextColor="#FFFFFF"
            placeholder="E-mail ou usuário"
            value={login}
            onChangeText={this.onChangeLogin}
            autoCapitalize='none'
          />

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholderTextColor="#FFFFFF"
            placeholder="Senha"
            value={senha}
            secureTextEntry={true}
            onChangeText={this.onChangeSenha}
            autoCapitalize='none'
          />

          <View style={styles.forgotPassContainer}>
            <TouchOpctyPrevSelect onPress={this.esqueciMinhaSenha}>
              <Text style={styles.linkText}>Esqueci minha senha</Text>
            </TouchOpctyPrevSelect>
          </View>

          <TouchOpctyPrevSelect
            onPress={this.login}>
            <Button
              title={btnLoginTitle}
              backgroundColor='#630851'
              titleSize={20}
            />
          </TouchOpctyPrevSelect>
        </View>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginUsuario: (cliente) => dispatch(loginUsuario(cliente)),
  };
};

export default connect(null, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'stretch',
    backgroundColor: '#ffc210',
  },
  logoContainer: {
    alignItems: 'center',
    marginTop: 60,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 20,
    paddingTop: 0,
    marginBottom: 40,
  },
  textInput: {
    marginBottom: 20,
    fontSize: 18,
    borderColor: '#FFFFFF',
    borderBottomWidth: 2,
    color: '#FFFFFF',
    height: 40,
  },
  forgotPassContainer: {
    marginLeft: 5,
  },
  linkText: {
    fontSize: 16,
    color: '#FFF',
    alignSelf: 'flex-start',
    marginBottom: 20,
  },
});
