/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { mostraEscondeSaldo } from '../actions/InitialScreenNavbarAction';
import { buscaExtrato } from '../actions/ExtractAction';

import InitialScreenNavbar from '../navbars/InitialScreenNavbar';
import AnimatedExtractItem from '../components/animated/AnimatedExtractItem';

class Extract extends Component<{}> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  componentDidMount() {
    const {id, token} = this.props.cliente;

    this.props.buscaTransacoes(id, token);
  }

  requireImg = (tipo) => {
    switch (tipo) {
      case 'Depósito':
        return require('../resources/deposito.png');
      case 'Saque':
        return require('../resources/imgSaque2.png');
      case 'TransferenciaSaida':
        return require('../resources/imgPagouPara.png');
      case 'TransferenciaEntrada':
        return require('../resources/imgRecebeuDe.png');
      default:
        return null;
    }
  };

  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => {
    const {tipo, valor, mensagem, data} = item;

    return (
      <AnimatedExtractItem {...item}>
        <View style={styles.extratoItem}>
          <View style={styles.conteudoExtrato}>
            <Image style={styles.imgTipoTransacao} source={this.requireImg(tipo)}/>
            <View style={styles.descricaoContainer}>
              <Text style={styles.valorTexto}>R$ {valor}</Text>
              <Text style={styles.tipoTransacaoTexto}>{mensagem}</Text>
            </View>
          </View>
          <Text style={styles.data}>{data}</Text>
        </View>
      </AnimatedExtractItem>
    );
  };

  mostraEscondeFunc = () => {
    this.props.mostraEscondeSaldo(this.props.mostraEsconde);
  };

  render() {
    const {nome, email, saldo, pontos, iniciais} = this.props.cliente;
    const {mostraEsconde, carteiras} = this.props;

    const listaExtrato = [
      {tipo: 'Depósito', valor: '23,01', mensagem: 'Depósito realizado!', data: '23/11/9999', status: 'Aguardando'},
      {tipo: 'Saque', valor: '23,02', mensagem: 'Saque realizado!', data: '23/11/9999'},
      {tipo: 'TransferenciaSaida', valor: '23,03', mensagem: 'Transferencia realizada!', data: '23/11/9999'},
      {tipo: 'TransferenciaEntrada', valor: '23,04', mensagem: 'Transferencia recebida!', data: '23/11/9999'},
      ];

    return (
      <View style={styles.container}>
        <InitialScreenNavbar
          nome={nome}
          email={email}
          saldo={carteiras.length > 0 ? carteiras[carteiras.length - 1].total : '0,00'}
          pontos={carteiras.length > 0 ? carteiras[carteiras.length - 1].pontos : '0,00'}
          navigator={this.props.navigator}
          mostraEsconde={mostraEsconde}
          iniciais={iniciais}
          mostraEscondeFunc={this.mostraEscondeFunc}
        />
        <Text style={styles.extratoTexto}>Extrato</Text>
        <FlatList
          data={listaExtrato}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const mapStateToProps = ({cliente, navBarTelaInicial, listaExtrato, carteiras}) => {
  const {mostraEsconde} = navBarTelaInicial;
  return {
    cliente,
    mostraEsconde,
    listaExtrato,
    carteiras,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    mostraEscondeSaldo: (mostraEsconde) => dispatch(mostraEscondeSaldo(mostraEsconde)),
    buscaTransacoes: (id, token) => dispatch(buscaExtrato(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Extract);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  extratoTexto: {
    padding: 15,
    paddingTop: 10,
    paddingBottom: 10,
    color: '#888888',
    fontSize: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  extratoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  conteudoExtrato: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgTipoTransacao: {
    width: 35,
    height: 35,
  },
  descricaoContainer: {
    paddingLeft: 10,
  },
  valorTexto: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
  },
  tipoTransacaoTexto: {
    fontSize: 12,
    color: '#AAAAAA',
  },
  data: {
    fontSize: 12,
    color: '#888888',
  },
});
