/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Platform,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';

export default class EmailSent extends Component<{}> {
  static navigatorStyle = {
    navBarHidden: true,
  };

  componentDidMount() {
  }


  mudaTelaLogin = () => {
    this.props.navigator.resetTo({
      screen: 'Login',
    });
  };

  render() {

    return (
      <View style={styles.container}>
        <TouchOpctyPrevSelect onPress={this.mudaTelaLogin}>
          <Image style={styles.imgMenu} source={require('../resources/fecha.png')}/>
        </TouchOpctyPrevSelect>

        <View style={styles.btnContainer}>

          <Image style={styles.imgLogo} source={require('../resources/email.png')}/>

          <Text style={styles.title}>Verifique seu e-mail e siga os passos para redefirnir sua senha</Text>

          <TouchOpctyPrevSelect
            onPress={this.mudaTelaLogin}>
            <Button
              title='FAZER LOGIN'
              backgroundColor='transparent'
              titleSize={16}
              borderWidth={2}
              borderColor='#630851'
              titleColor='#630851'
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
    paddingTop: Platform.OS === 'ios' ? 25 : 15,
  },
  headerContainer: {
    backgroundColor: '#FFFFFF',
  },
  imgMenu: {
    width: 20,
    height: 20,
    margin: 5,
  },
  imgLogo: {
    width: 60,
    height: 60,
    marginBottom: 20,
    alignSelf: 'center',
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 15,
    fontSize: 14,
    borderColor: '#FFFFFF',
    color: '#000000',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
    marginBottom: 30,
    textAlign: 'center',
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
