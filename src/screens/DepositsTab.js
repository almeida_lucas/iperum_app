import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';
import { Navigation } from 'react-native-navigation';

import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import AnimatedExtractItem from '../components/animated/AnimatedExtractItem';

export default TabDepositos = ({}) => {
const listaExtrato = [{tipo: 'Depósito', valor: '23,00', data: '23/11/9999', status: 'Aguardando'}];
  const modalDeposito = () => Navigation.showModal({
    component: {
      id: 'ModalDeposito',
      name: 'ModalDeposito',
      passProps: {
        // chat_id,
        // user_token,
        user_id: 1,
        token: '',
      },
      options: {
        screenBackgroundColor: 'transparent',
        modalPresentationStyle: 'overCurrentContext',
      },
    },
  });

  const keyExtractor = (item, index) => index.toString();

  const renderItem = ({item}) => {
    const {valor, data, status} = item;

    return (
      <AnimatedExtractItem {...item}>
        <View style={styles.depositoItem}>
          <View style={styles.conteudoDeposito}>
            <Image style={styles.imgDeposito} source={require('../resources/deposito.png')}/>
            <View style={styles.descricaoContainer}>
              <Text style={styles.valorTexto}>R$ {valor}</Text>
              <Text style={styles.data}>{data}</Text>
            </View>
          </View>
          <View style={[styles.statusDepositoView, {backgroundColor: status === 'Aguardando' ? 'orange' : '#5bab2e'}]}>
          <Text
            style={styles.statusDepositoTexto}>{status}</Text>
          </View>
        </View>
      </AnimatedExtractItem>
    );
  };

  const listaSolicitacoes = listaExtrato.filter(solicitacao => solicitacao.tipo === 'Depósito');

  return (
    <View style={styles.container}>
      <View style={styles.btContainer}>
        <TouchOpctyPrevSelect
          onPress={modalDeposito}>
          <Button
            title='Depositar'
            subTitle='na Ipeconta'
            backgroundColor='#5bab2e'
            alignText='flex-start'
            btImg='deposito'
            btImgStyle={{height: 60, width: 60, opacity: 1, top: 5, right: 10}}
          />
        </TouchOpctyPrevSelect>
      </View>
      {listaSolicitacoes.length > 0 ?
        <View style={styles.depositContainer}>
          <Text style={styles.depositoTexto}>Depósitos</Text>
          <FlatList
            data={listaSolicitacoes}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />
        </View>
        :
        <View style={{flexDirection: 'row', padding: 15, paddingRight: 30, paddingTop: 10}}>
          <View style={{flex: 1, paddingTop: 15}}>
            <Text style={{fontSize: 15}}>Nenhum depósito por enquanto...</Text>
            <Text style={{fontSize: 15}}>Toque no botao acima para depositar</Text>
          </View>
          <Image style={{width: 30, height: 50}} source={require('../resources/seta-cima.png')}/>
        </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  btContainer: {
    alignItems: 'stretch',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  depositContainer: {
    flex: 1
  },
  depositoTexto: {
    padding: 15,
    paddingTop: 10,
    paddingBottom: 10,
    color: '#888888',
    fontSize: 15,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  depositoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderColor: '#e0e0e0',
  },
  conteudoDeposito: {
    flexDirection: 'row',
  },
  imgDeposito: {
    width: 35,
    height: 35,
  },
  descricaoContainer: {
    paddingLeft: 5,
  },
  valorTexto: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
  },
  statusDepositoView: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 20,
  },
  statusDepositoTexto: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  data: {
    fontSize: 12,
    color: '#888888',
  },
});
