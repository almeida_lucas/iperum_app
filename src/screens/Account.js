import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';

import InitialScreenNavbar from '../navbars/InitialScreenNavbar';
import TabDepositos from './DepositsTab';
import TabSaques from './WithdrawTab';

import { connect } from 'react-redux';
import { mostraEscondeSaldo } from '../actions/InitialScreenNavbarAction';
import { buscaSolicitacoes, buscaContasCadastradas } from '../actions/AccountAction';
import { buscaCliente } from '../actions/ClientAction';
import { buscaExtrato } from '../actions/ExtractAction';
import { BUSCA_CLIENTE_ERRO, BUSCA_CLIENTE_SUCESSO, LONG, tokenExpirado } from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';

const initialLayout = {
  height: Dimensions.get('screen').height,
  width: Dimensions.get('screen').width,
};

type Props = {};

class Account extends Component<Props> {

  state = {
    index: 0,
    routes: [
      {key: 'Depositos', title: 'Depósitos'},
      {key: 'Saques', title: 'Saques'},
    ],
  };

  tokenValido = () => {
    const {id, token} = this.props.cliente;
    this.props.buscaSolicitacoes(id, token);
    this.props.buscaContasCadastradas(id, token);
  };

  componentDidMount() {
    const {id, token} = this.props.cliente;
    /*this.props.buscaCliente(id, token)
      .then(({type, error}) => {
        if (type === BUSCA_CLIENTE_SUCESSO) this.tokenValido();
        else if (type === BUSCA_CLIENTE_ERRO) {
          clearInterval(this.props.interval);
          tokenExpirado('Erro', error, LONG);
        }
      });*/
  }

  renderHeader = props => (
    <TabBar
      style={{backgroundColor: '#FFFFFF'}}
      labelStyle={{color: '#000000', fontSize: 12}}
      {...props}
    />
  );

  handleIndexChange = index => this.setState({index});

  renderScene = ({route}) => {
    switch (route.key) {
      case 'Depositos':
        return <TabDepositos {...this.props}/>;
      case 'Saques':
        return <TabSaques {...this.props} />;
      default:
        return null;
    }
  };

  mostraEscondeFunc = () => {
    this.props.mostraEscondeSaldo(this.props.mostraEsconde);
  };

  render() {
    const {nome, email, saldo, pontos, iniciais} = this.props.cliente;
    const {mostraEsconde, carteiras} = this.props;

    return (
      <View style={{flex: 1}}>
        <InitialScreenNavbar
          nome={nome}
          email={email}
          saldo={carteiras.length > 0 ? carteiras[carteiras.length - 1].total : '0,00'}
          pontos={carteiras.length > 0 ? carteiras[carteiras.length - 1].pontos : '0,00'}
          navigator={this.props.navigator}
          mostraEsconde={mostraEsconde}
          iniciais={iniciais}
          mostraEscondeFunc={this.mostraEscondeFunc}
        />
        <TabView
          navigationState={this.state}
          renderScene={this.renderScene}
          renderHeader={this.renderHeader}
          onIndexChange={this.handleIndexChange}
        />
      </View>
    );
  }
}

const mapStateToProps = ({cliente, navBarTelaInicial, conta, carteiras, listaExtrato, interval}) => {
  const {mostraEsconde} = navBarTelaInicial;
  const {solicitacoes, contas} = conta;
  return {
    cliente,
    mostraEsconde,
    solicitacoes,
    contas,
    carteiras,
    listaExtrato,
    interval,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    mostraEscondeSaldo: (mostraEsconde) => dispatch(mostraEscondeSaldo(mostraEsconde)),
    buscaSolicitacoes: (id, token) => dispatch(buscaSolicitacoes(id, token)),
    buscaContasCadastradas: (id, token) => dispatch(buscaContasCadastradas(id, token)),
    buscaTransacoes: (id, token) => dispatch(buscaExtrato(id, token)),
    buscaCliente: (id, token) => dispatch(buscaCliente(id, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
