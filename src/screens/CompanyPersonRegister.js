/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';
import { verificaEmail } from '../actions/RegisterAction';
import { connect } from 'react-redux';
import InputError from '../components/InputError';
import { LONG, VERIFICA_EMAIL_ERRO, VERIFICA_EMAIL_SUCESSO } from '../contants';
import { ShowNativeMessage } from '../components/NativeMessage';

class CompanyPersonRegister extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
  };

  constructor() {
    super();

    this.state = {
      nome: '',
      email: '',
      celular: '',
      msgErro: '',
      msgErrEmail: '',
    };
  }

  componentDidMount() {
    this.setState({
      nome: '',
      email: '',
      celular: '',
      msgErro: '',
      msgErrEmail: '',
    });
  }

  continuar = () => {
    const {nome, email, celular} = this.state;

    if (nome === '' || email === '' || celular === '') {
      this.setState({msgErro: '*Todos os campos devem ser preenchidos!'});
    } else {
      this.verificaEmail()
        .then(({type}) => {
          if (type === VERIFICA_EMAIL_ERRO) ShowNativeMessage('Erro', 'Insira um EMAIL válido para seguir', LONG);
          else if (type === VERIFICA_EMAIL_SUCESSO) {
            const cliente = {
              ...this.props.cliente,
              nome,
              email,
              tipo_cliente: 'J',
              telefone: celular,
            };

            this.props.navigator.push({
              screen: 'CadastroUsuario',
              passProps: {
                cliente,
              },
              backButtonTitle: '',
            });
          }
        });
    }
  };

  onChangeEmail = (email) => this.setState({email});
  onChangeTelefone = (celular) => this.setState({celular});

  verificaEmail = () => this.props.verificaEmail(this.state.email)
    .then(({type, error, data}) => {
      if (type === VERIFICA_EMAIL_ERRO) this.setState({msgErrEmail: error});
      else if (type === VERIFICA_EMAIL_SUCESSO) this.setState({msgErrEmail: ''});
      return {type, error, data};
    });

  render() {
    const {nome, email, celular, msgErro, msgErrEmail} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.btnContainer}>
          <Text style={styles.title}>Agora um pouco sobre você...</Text>

          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            placeholder="Nome completo"
            value={nome}
            onChangeText={(nome) => this.setState({nome})}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
          />

          <InputError
            onChangeText={this.onChangeEmail}
            msgErr={msgErrEmail}
            placeholder='Email'
            value={email}
            onBlur={this.verificaEmail}
            autoCapitalize='none'
            returnKeyLabel='Próximo'
            maxLength={255}
          />

          <InputError
            onChangeText={this.onChangeTelefone}
            type='cel-phone'
            placeholder='Celular (com DDD)'
            value={celular}
            returnKeyLabel='Próximo'
            maxLength={15}
          />

          <Text style={styles.msgErro}>{msgErro}</Text>

          <TouchOpctyPrevSelect
            onPress={this.continuar}>
            <Button
              title='CONTINUAR'
              backgroundColor='#ffc210'
              titleSize={20}
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>

      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    verificaEmail: (email) => dispatch(verificaEmail(email)),
  };
};

export default connect(null, mapDispatchToProps)(CompanyPersonRegister);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 15,
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: '#ffc210',
    color: '#000000',
    height: 40,
  },
  title: {
    fontSize: 26,
    color: '#000000',
    marginBottom: 30,
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
