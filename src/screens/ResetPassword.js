/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import Button from '../components/Button';
import TouchOpctyPrevSelect from '../components/TouchOpctyPrevSelect';
import FooterTerms from '../components/FooterTerms';
import { resetSenha } from '../actions/ClientAction';
import { connect } from 'react-redux';
import InputError from '../components/InputError';
import { LONG, RESET_SENHA_ERRO, RESET_SENHA_SUCESSO } from '../contants';
import ActivityIndicatorModal from '../modals/ActivityIndicatorModal';
import { ShowNativeMessage } from '../components/NativeMessage';

class ResetPassword extends Component<{}> {
  static navigatorStyle = {
    navBarTransparent: true,
    topBarElevationShadowEnabled: false,
    navBarButtonColor: '#ffc210',
    navBarBackgroundColor: '#ffffff',
  };

  state = {
    usuario: '',
    email: '',
    msgErro: '',
    continueDisabled: false,
    continuePressed: false,
  };

  componentDidMount() {
    this.setState({
      email: '',
      msgErro: '',
    });
  }

  onChangeEmail = (email) => this.setState({email, msgErro: ''});

  continuar = () => {
    const {email} = this.state;
    this.setState({continuePressed: true});

    if (email === '') {
      this.setState({msgErro: '*Todos os campos devem ser preenchidos!'});
      this.setState({continueDisabled: false});
    } else {
      this.props.resetSenha(email)
        .then(({type, error, status}) => {
          this.setState({continueDisabled: false});
          if (type === RESET_SENHA_SUCESSO) {
            this.props.navigator.push({
              screen: 'EmailSent',
              backButtonTitle: '',
            });
            ShowNativeMessage('Reset senha', status, LONG);
          } else if (type === RESET_SENHA_ERRO) {
            ShowNativeMessage('Erro', error, LONG);
          }
        });
    }
  };

  disableContinue = () => {
    this.setState(
      {continueDisabled: true},
      () => setTimeout(() => {
        if (this.state.continuePressed) this.setState({continuePressed: false});
        else this.setState({continueDisabled: false});
      }, 500));
  };

  render() {
    const {email, msgErro, continueDisabled} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.btnContainer}>
          <Text style={styles.title}>Digite o seu e-mail cadastrado para redefinir a senha</Text>

          <InputError
            onChangeText={this.onChangeEmail}
            placeholder='Digite seu e-mail cadastrado'
            value={email}
            maxLength={255}
          />

          <Text style={styles.msgErro}>{msgErro}</Text>

          <TouchOpctyPrevSelect
            onPressOut={this.disableContinue}
            onPress={this.continuar}
            disabled={continueDisabled}>
            <Button
              title='CONTINUAR'
              backgroundColor='#ffc210'
              titleSize={20}
            />
          </TouchOpctyPrevSelect>
        </View>

        <FooterTerms/>
        {/*<ActivityIndicatorModal visible={continueDisabled}/>*/}
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetSenha: (email) => dispatch(resetSenha(email)),
  };
};

export default connect(null, mapDispatchToProps)(ResetPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: 15,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textInput: {
    marginBottom: 15,
    fontSize: 14,
    borderColor: '#FFFFFF',
    color: '#000000',
  },
  title: {
    fontSize: 26,
    color: '#000000',
    marginBottom: 30,
  },
  msgErro: {
    fontSize: 12,
    color: 'red',
    marginBottom: 15,
  },
});
