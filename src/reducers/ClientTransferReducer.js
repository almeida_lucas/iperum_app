import {
  LISTA_CLIENTES_SUCESSO,
  LISTA_CLIENTES_ERRO, LOGIN_USUARIO,
} from '../contants';

export default (state = [], action = null) => {
  const {type, listaClientes} = action;
  switch (type) {
    case LISTA_CLIENTES_SUCESSO:
      return listaClientes;
    case LOGIN_USUARIO:
      return [];
    default:
      return state;
  }
}