import {
  BUSCA_SOLICITACOES_SUCESSO,
  BUSCA_SOLICITACOES_ERRO,
  BUSCA_CONTAS_SUCESSO,
  BUSCA_CONTAS_ERRO, LOGIN_USUARIO,
} from '../contants';

export default (state = {solicitacoes: [], contas: []}, action = null) => {
  const {type, solicitacoes, contas, erro} = action;
  switch (type) {
    case BUSCA_SOLICITACOES_SUCESSO:
      return {...state, solicitacoes};
    case BUSCA_CONTAS_SUCESSO:
      return {...state, contas};
    case LOGIN_USUARIO:
      return {solicitacoes: [], contas: []};
    default:
      return state;
  }
}