import {
  TOKEN_TRANSF_SUCESSO, TOKEN_TRANSF_ERRO, LOGIN_USUARIO,
} from '../contants';

export default (state = '', action = null) => {
  const {type, token} = action;
  switch (type) {
    case TOKEN_TRANSF_SUCESSO:
      return token;
    case LOGIN_USUARIO:
      return '';
    default:
      return state;
  }
}