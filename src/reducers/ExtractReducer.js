import {
  LISTA_EXTRATO_SUCESSO,
  LISTA_EXTRATO_ERRO, LOGIN_USUARIO,
} from '../contants';

export default (state = [], action = null) => {
  const {type, listaExtrato} = action;
  switch (type) {
    case LISTA_EXTRATO_SUCESSO:
      return listaExtrato;
    case LOGIN_USUARIO:
      return [];
    default:
      return state;
  }
}