import {
  LOGIN_USUARIO,
  MOSTRA_ESCONDE_SALDO,
} from '../contants';

export default (state = {mostraEsconde: true}, action = null) => {
  const {type, mostraEsconde} = action;
  switch (type) {
    case MOSTRA_ESCONDE_SALDO:
      return {mostraEsconde};
    case LOGIN_USUARIO:
      return {mostraEsconde: true};
    default:
      return state;
  }
}