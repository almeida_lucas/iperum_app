import { combineReducers } from 'redux';

import navBarTelaInicial from './InitialScreenNavbarReducer';
import conta from './AccountReducer';
import cliente from './ClientReducer';
import listaClientes from './ClientTransferReducer';
import listaExtrato from './ExtractReducer';
import carteiras from './WalletReducer';
import tokenTransf from './TokenTransfReducer';
import interval from './IntervalReducer';

export default combineReducers({
  navBarTelaInicial,
  conta,
  cliente,
  listaClientes,
  listaExtrato,
  carteiras,
  tokenTransf,
  interval,
});