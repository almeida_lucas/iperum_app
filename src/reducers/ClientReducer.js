import {
  ATUALIZA_CLIENTE_SUCESSO,
  BUSCA_CLIENTE_SUCESSO,
  LOGIN_USUARIO,
  SALVA_CLIENTE_SUCESSO,
} from '../contants';

export default (state = {cliente: null}, action = null) => {
  const {type, cliente} = action;
  switch (type) {
    case BUSCA_CLIENTE_SUCESSO:
    case SALVA_CLIENTE_SUCESSO:
    case ATUALIZA_CLIENTE_SUCESSO:
      return {...state, ...cliente};
    case LOGIN_USUARIO:
      return cliente;
    default:
      return state;
  }
}