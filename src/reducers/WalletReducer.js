import {
  LISTA_CARTEIRAS_SUCESSO,
  LISTA_CARTEIRAS_ERRO, LOGIN_USUARIO,
} from '../contants';

export default (state = [], action = null) => {
  const {type, carteiras} = action;
  switch (type) {
    case LISTA_CARTEIRAS_SUCESSO:
      return carteiras;
    case LOGIN_USUARIO:
      return [];
    default:
      return state;
  }
}