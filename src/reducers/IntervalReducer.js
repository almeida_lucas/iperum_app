import {
  START_INTERVAL, STOP_INTERVAL,
} from '../contants';

export default (state = null, action = null) => {
  const {type, interval} = action;
  switch (type) {
    case START_INTERVAL:
      return interval;
    default:
      return state;
  }
}