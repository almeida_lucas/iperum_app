import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { default as promise } from 'redux-promise';

import { Navigation } from 'react-native-navigation';
import SplashScreen from './src/screens/SplashScreen';
import Initial from './src/screens/Initial';
import Login from './src/screens/Login';
import Welcome from './src/screens/Welcome';
import CadastroEmpresa from './src/screens/CompanyRegister';
import CadastroPessoa from './src/screens/PersonRegister';
import CadastroUsuario from './src/screens/UserRegister';
import Pagamento from './src/screens/Payment';
import Extrato from './src/screens/Extract';
import Conta from './src/screens/Account';
import EsqueciMinhaSenha from './src/screens/ResetPassword';

import PresentationModal from './src/modals/PresentationModal';
import ModalEnviarPara from './src/modals/SendToModal';
import ModalPerfil from './src/modals/PerfilModal';
import CadastroEmpresaPessoa from './src/screens/CompanyPersonRegister';
import ModalCadastraContaSaque from './src/modals/RegisterWithdrawalAccModal';
import WithdrawModal from './src/modals/WithdrawModal';
import ModalDeposito from './src/modals/DepositModal';
import ConfirmDepositModal from './src/modals/ConfirmDepositModal';
import ModalConfirmaSaque from './src/modals/ConfirmWithdrawModal';
import ModalSaqueAgendado from './src/modals/WithdrawalScheduledModal';
import ModalPagarPara from './src/modals/PayToModal';
import ConfirmPaymentModal from './src/modals/ConfirmPaymentModal';
import ModalPagamentoSucesso from './src/modals/PaymentSuccessModal';
import ModalPoliticaPrivacidade from './src/modals/PrivacyPoliticsModal';
import ModalTermosServicos from './src/modals/ServicesTermsModal';
import ModalEditaPerfil from './src/modals/EditPerfilModal';
import SelectListModal from './src/modals/SelectListModal';
import EditPassword from './src/modals/EditPassword';

import reducers from './src/reducers';
import EmailSent from './src/screens/EmailSent';

const store = createStore(reducers, applyMiddleware(promise));

Navigation.registerComponentWithRedux('SplashScreen', () => SplashScreen, Provider, store);
Navigation.registerComponent('Initial', () => Initial);
Navigation.registerComponentWithRedux('Login', () => Login, Provider, store);
Navigation.registerComponent('Welcome', () => Welcome);
Navigation.registerComponentWithRedux('CadastroEmpresa', () => CadastroEmpresa, Provider, store);
Navigation.registerComponentWithRedux('CadastroPessoa', () => CadastroPessoa, Provider, store);
Navigation.registerComponentWithRedux('CadastroEmpresaPessoa', () => CadastroEmpresaPessoa, Provider, store);
Navigation.registerComponentWithRedux('CadastroUsuario', () => CadastroUsuario, Provider, store);
Navigation.registerComponentWithRedux('Pagamento', () => Pagamento, Provider, store);
Navigation.registerComponentWithRedux('Extrato', () => Extrato, Provider, store);
Navigation.registerComponentWithRedux('Conta', () => Conta, Provider, store);
Navigation.registerComponentWithRedux('EsqueciMinhaSenha', () => EsqueciMinhaSenha, Provider, store);
Navigation.registerComponent('EmailSent', () => EmailSent);

Navigation.registerComponent('PresentationModal', () => PresentationModal);
Navigation.registerComponentWithRedux('ModalEnviarPara', () => ModalEnviarPara, Provider, store);
Navigation.registerComponentWithRedux('ModalPerfil', () => ModalPerfil, Provider, store);
Navigation.registerComponentWithRedux('ModalCadastraContaSaque', () => ModalCadastraContaSaque, Provider, store);
Navigation.registerComponentWithRedux('ModalDeposito', () => ModalDeposito, Provider, store);
Navigation.registerComponent('ConfirmDepositModal', () => ConfirmDepositModal);
Navigation.registerComponent('WithdrawModal', () => WithdrawModal);
Navigation.registerComponentWithRedux('ModalConfirmaSaque', () => ModalConfirmaSaque, Provider, store);
Navigation.registerComponent('ModalSaqueAgendado', () => ModalSaqueAgendado);
Navigation.registerComponentWithRedux('ModalPagarPara', () => ModalPagarPara, Provider, store);
Navigation.registerComponentWithRedux('ConfirmPaymentModal', () => ConfirmPaymentModal, Provider, store);
Navigation.registerComponentWithRedux('ModalPagamentoSucesso', () => ModalPagamentoSucesso, Provider, store);
Navigation.registerComponent('ModalPoliticaPrivacidade', () => ModalPoliticaPrivacidade);
Navigation.registerComponent('ModalTermosServicos', () => ModalTermosServicos);
Navigation.registerComponentWithRedux('ModalEditaPerfil', () => ModalEditaPerfil, Provider, store);
Navigation.registerComponent('SelectListModal', () => SelectListModal);
Navigation.registerComponentWithRedux('EditPassword', () => EditPassword, Provider, store);


Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "SplashScreen",
      },
    },
  });
});
